

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;


internal class SkinShaderGUI_Layer : ShaderGUI
{
    private static class Styles
    {
        public static GUIStyle optionsButton = "PaneOptions";

        public static string emptyTootip = "";
        public static GUIContent albedoText = new GUIContent("Albedo", "Albedo (RGB) and Transparency (A)");
        public static GUIContent specularText = new GUIContent("Specular[光滑度(R) 厚度图(G) 附加阴影(B)]", "光滑度(R) 曲率(G) 附加阴影(B)");
        public static GUIContent bumpMapText = new GUIContent("NormalMap", "Normal Map");
        public static GUIContent lookupText = new GUIContent("LUT", "Lookup texture");
        public static GUIContent fresnelText = new GUIContent("fresnel", "fresnel");
        public static GUIContent shadowIntensityText = new GUIContent("shadowIntensity", "shadowIntensity");
        public static GUIContent extraShadeRangeText = new GUIContent("extraShadeRange", "extraShadeRange");

        public static GUIContent shadowColorText = new GUIContent("部分阴影颜色", "_ShadowColor");
        public static GUIContent shadeColorText = new GUIContent("ShadeColor", "ShadeColor");
        public static GUIContent specularColorText = new GUIContent("高光颜色", "ShadeColor");
        public static GUIContent NN4AmbientTintText = new GUIContent("环境光", "ShadeColor");
                
         #region Layer GUIContent
            public static GUIContent layer0TexText = new GUIContent("Layer0", "");            
            public static GUIContent layer0ColorText = new GUIContent("Layer0Color", "");
            public static GUIContent layer0SmoothnessText = new GUIContent("Layer0Smoothness", "Layer0 Smoothness");
            public static GUIContent layer0MetallicText = new GUIContent("Layer0Metallic", "Layer0 Metallic");        
            public static GUIContent layer0UVSetText = new GUIContent("Layer0UVSet", "Layer0 UVSet");
            public static GUIContent layer0NormalText = new GUIContent("Layer0Normal", "Layer0 Normal Texture");
            public static GUIContent layer0NormalModeText = new GUIContent("NormalBlendMode", ""); 

            public static GUIContent layer1TexText = new GUIContent("Layer1", "");            
            public static GUIContent layer1ColorText = new GUIContent("Layer1Color", "");
            public static GUIContent layer1SmoothnessText = new GUIContent("Layer1Smoothness", "");
            public static GUIContent layer1MetallicText = new GUIContent("Layer1Metallic", "");
            public static GUIContent layer1NormalModeText = new GUIContent("NormalBlendMode", "");
            public static GUIContent layer1UVSetText = new GUIContent("Layer1UVSet", "");
            public static GUIContent layer1NormalText = new GUIContent("Layer1Normal", "");
            public static GUIContent layer1CutoffText = new GUIContent("Alpha Cutoff", "");

            public static GUIContent layer2TexText = new GUIContent("Layer2", "");            
            public static GUIContent layer2ColorText = new GUIContent("Layer2Color", "");
            public static GUIContent layer2SmoothnessText = new GUIContent("Layer2Smoothness", "Layer2 Smoothness");
            public static GUIContent layer2MetallicText = new GUIContent("Layer2Metallic", "Layer2 Metallic");        
            public static GUIContent layer2UVSetText = new GUIContent("Layer2UVSet", "Layer2 UVSet");
            public static GUIContent layer2NormalText = new GUIContent("Layer2Normal", "Layer2 Normal Texture");
            public static GUIContent layer2NormalModeText = new GUIContent("NormalBlendMode", "");
                        
            public static GUIContent layer3TexText = new GUIContent("Layer3", "");            
            public static GUIContent layer3ColorText = new GUIContent("Layer3Color", "");
            public static GUIContent layer3SmoothnessText = new GUIContent("Layer3Smoothness", "");
            public static GUIContent layer3MetallicText = new GUIContent("Layer3Metallic", "");
            public static GUIContent layer3UVSetText = new GUIContent("Layer3UVSet", "");
            public static GUIContent layer3NormalText = new GUIContent("Layer3Normal", "");
            public static GUIContent layer3NormalModeText = new GUIContent("NormalBlendMode", "");
            

            public static GUIContent layer4TexText = new GUIContent("Layer4", "");            
            public static GUIContent layer4ColorText = new GUIContent("Layer4Color", "");
            public static GUIContent layer4SmoothnessText = new GUIContent("Layer4Smoothness", "");
            public static GUIContent layer4MetallicText = new GUIContent("Layer4Metallic", "");
            public static GUIContent layer4UVSetText = new GUIContent("Layer4UVSet", "");
            public static GUIContent layer4NormalText = new GUIContent("Layer4Normal", "");
            public static GUIContent layer4NormalModeText = new GUIContent("NormalBlendMode", "");
            

            public static GUIContent layer5TexText = new GUIContent("Layer5", "Layer5 Texture");            
            public static GUIContent layer5ColorText = new GUIContent("Layer5Color", "Layer5 Color");
            public static GUIContent layer5SmoothnessText = new GUIContent("Layer5Smoothness", "Layer5 Smoothness");
            public static GUIContent layer5MetallicText = new GUIContent("Layer5Metallic", "Layer5 Metallic");
            public static GUIContent layer5UVSetText = new GUIContent("Layer5UVSet", "Layer5 UVSet");
            public static GUIContent layer5NormalText = new GUIContent("Layer5Normal", "Layer5 Normal Texture");
            public static GUIContent layer5NormalModeText = new GUIContent("NormalBlendMode", "");
            

            public static GUIContent layer6TexText = new GUIContent("Layer6", "Layer6 Texture");            
            public static GUIContent layer6ColorText = new GUIContent("Layer6Color", "Layer6 Color");
            public static GUIContent layer6SmoothnessText = new GUIContent("Layer6Smoothness", "Layer6 Smoothness");
            public static GUIContent layer6MetallicText = new GUIContent("Layer6Metallic", "Layer6 Metallic");
            public static GUIContent layer6UVSetText = new GUIContent("Layer6UVSet", "Layer6 UVSet");
            public static GUIContent layer6NormalText = new GUIContent("Layer6Normal", "Layer6 Normal Texture");
            public static GUIContent layer6NormalModeText = new GUIContent("NormalBlendMode", "");
            
            
            public static GUIContent layer7TexText = new GUIContent("Layer7", "Layer7 Texture");            
            public static GUIContent layer7ColorText = new GUIContent("Layer7Color", "Layer7 Color");
            public static GUIContent layer7SmoothnessText = new GUIContent("Layer7Smoothness", "Layer7 Smoothness");
            public static GUIContent layer7MetallicText = new GUIContent("Layer7Metallic", "Layer7 Metallic");
            public static GUIContent layer7UVSetText = new GUIContent("Layer7UVSet", "Layer7 UVSet");
            public static GUIContent layer7NormalText = new GUIContent("Layer7Normal", "Layer7 Normal Texture");
            public static GUIContent layer7NormalModeText = new GUIContent("NormalBlendMode", "");
            #endregion


        public static string primaryMapsText = "主要贴图";
        public static string otherText = "其他参数";
        public static string colorText = "颜色参数";
        public static string layerText = "妆容层级";
        
    }

    
    MaterialProperty albedoMap = null;    
    // --
    MaterialProperty specularTex = null;
    
    MaterialProperty bumpMap = null;
    // MaterialProperty bumpMapScale = null;   
    MaterialProperty lutTex = null;

    // -- 
    MaterialProperty fresnel = null;
    MaterialProperty shadowIntensity = null;
    MaterialProperty extraShadeRange = null;
    

    // color 
    MaterialProperty _BaseColor = null;
    MaterialProperty _ShadowColor = null;
    MaterialProperty _ShadeColor = null;
    MaterialProperty _SpecularColor = null;
    MaterialProperty _NN4AmbientTint = null;

    
     #region AddLayer
            // Layer
    MaterialProperty layer0Tex;            
    MaterialProperty layer0Color;
    MaterialProperty layer0Smoothness;
    MaterialProperty layer0Metallic;
    MaterialProperty layer0UVSet;
    MaterialProperty layer0Normal;
    MaterialProperty layer0NormalScale;
    MaterialProperty layer0NormalMode;

    MaterialProperty layer1Tex;            
    MaterialProperty layer1Color;
    MaterialProperty layer1Smoothness;
    MaterialProperty layer1Metallic;
    MaterialProperty layer1UVSet;
    MaterialProperty layer1Normal;
    MaterialProperty layer1NormalMode; //_Layer1NormalMode
    MaterialProperty layer1NormalScale;
    MaterialProperty layer1Cutoff;

    MaterialProperty layer2Tex;            
    MaterialProperty layer2Color;
    MaterialProperty layer2Smoothness;
    MaterialProperty layer2Metallic;
    MaterialProperty layer2UVSet;
    MaterialProperty layer2Normal;
    MaterialProperty layer2NormalScale;
    MaterialProperty layer2NormalMode;

    MaterialProperty layer3Tex;            
    MaterialProperty layer3Color;
    MaterialProperty layer3Smoothness;
    MaterialProperty layer3Metallic;
    MaterialProperty layer3UVSet;
    MaterialProperty layer3Normal;
    MaterialProperty layer3NormalScale;
    MaterialProperty layer3NormalMode;

    MaterialProperty layer4Tex;            
    MaterialProperty layer4Color;
    MaterialProperty layer4Smoothness;
    MaterialProperty layer4Metallic;
    MaterialProperty layer4UVSet;
    MaterialProperty layer4Normal;
    MaterialProperty layer4NormalScale;
    MaterialProperty layer4NormalMode;

    MaterialProperty layer5Tex;            
    MaterialProperty layer5Color;
    MaterialProperty layer5Smoothness;
    MaterialProperty layer5Metallic;
    MaterialProperty layer5UVSet;
    MaterialProperty layer5Normal;
    MaterialProperty layer5NormalScale;
    MaterialProperty layer5NormalMode;

    MaterialProperty layer6Tex;            
    MaterialProperty layer6Color;
    MaterialProperty layer6Smoothness;
    MaterialProperty layer6Metallic;
    MaterialProperty layer6UVSet;
    MaterialProperty layer6Normal;
    MaterialProperty layer6NormalScale;
    MaterialProperty layer6NormalMode;

    MaterialProperty layer7Tex;            
    MaterialProperty layer7Color;
    MaterialProperty layer7Smoothness;
    MaterialProperty layer7Metallic;
    MaterialProperty layer7UVSet;
    MaterialProperty layer7Normal;
    MaterialProperty layer7NormalScale;
    MaterialProperty layer7NormalMode;

            #endregion
    
    
    MaterialEditor m_MaterialEditor;    

    bool m_FirstTimeApply = true;

    public void FindProperties(MaterialProperty[] props)
    {
        // texture
        albedoMap = FindProperty("_MainTexBase", props);
        specularTex = FindProperty("_SpecularTex", props);
        bumpMap = FindProperty("_NormalMapBase", props);
        // bumpMapScale = FindProperty("_NormalMapScale", props);
        lutTex = FindProperty("_LUTTex", props);

        // float
        fresnel = FindProperty("_Fresnel", props);
        shadowIntensity = FindProperty("_ShadowIntensity", props);
        extraShadeRange = FindProperty("_ExtraShadeRange", props);

        // color
        _BaseColor = FindProperty("_MainTexBaseColor", props);
        _ShadowColor = FindProperty("_ShadowColor", props);
        _ShadeColor = FindProperty("_ShadeColor", props);
        _SpecularColor = FindProperty("_SpecularColorBase", props);
        _NN4AmbientTint = FindProperty("_NN4AmbientTint", props);        

                        // Skin Layer 
        #region Skin Layer
                layer0Tex = FindProperty("_Layer0Tex", props, false);                
                layer0UVSet = FindProperty("_Layer0UVSet", props, false);
                layer0Normal = FindProperty("_Layer0Normal", props, false);
                layer0NormalScale = FindProperty("_Layer0NormalScale", props, false);
                layer0Color = FindProperty("_Layer0Color", props, false);
                layer0Smoothness = FindProperty("_Layer0Smoothness", props, false);
                layer0Metallic = FindProperty("_Layer0Metallic", props, false);
                layer0NormalMode = FindProperty("_Layer0NormalMode", props, false);


                layer1Tex = FindProperty("_Layer1Tex", props, false);                
                layer1UVSet = FindProperty("_Layer1UVSet", props, false);
                layer1Normal = FindProperty("_Layer1Normal", props, false);
                layer1NormalScale = FindProperty("_Layer1NormalScale", props, false);
                layer1NormalMode = FindProperty("_Layer1NormalMode", props, false);
                layer1Color = FindProperty("_Layer1Color", props, false);
                layer1Smoothness = FindProperty("_Layer1Smoothness", props, false);
                layer1Metallic = FindProperty("_Layer1Metallic", props, false);
                layer1Cutoff = FindProperty("_Layer1Cutoff", props, false);

                layer2Tex = FindProperty("_Layer2Tex", props, false);                
                layer2UVSet = FindProperty("_Layer2UVSet", props, false);
                layer2Normal = FindProperty("_Layer2Normal", props, false);
                layer2NormalScale = FindProperty("_Layer2NormalScale", props, false);
                layer2Color = FindProperty("_Layer2Color", props, false);
                layer2Smoothness = FindProperty("_Layer2Smoothness", props, false);
                layer2Metallic = FindProperty("_Layer2Metallic", props, false);
                layer2NormalMode = FindProperty("_Layer2NormalMode", props, false);

                layer3Tex = FindProperty("_Layer3Tex", props, false);                
                layer3UVSet = FindProperty("_Layer3UVSet", props, false);
                layer3Normal = FindProperty("_Layer3Normal", props, false);
                layer3NormalScale = FindProperty("_Layer3NormalScale", props, false);
                layer3Color = FindProperty("_Layer3Color", props, false);
                layer3Smoothness = FindProperty("_Layer3Smoothness", props, false);
                layer3Metallic = FindProperty("_Layer3Metallic", props, false);
                layer3NormalMode = FindProperty("_Layer3NormalMode", props, false);

                layer4Tex = FindProperty("_Layer4Tex", props, false);                
                layer4UVSet = FindProperty("_Layer4UVSet", props, false);
                layer4Normal = FindProperty("_Layer4Normal", props, false);
                layer4NormalScale = FindProperty("_Layer4NormalScale", props, false);
                layer4Color = FindProperty("_Layer4Color", props, false);
                layer4Smoothness = FindProperty("_Layer4Smoothness", props, false);
                layer4Metallic = FindProperty("_Layer4Metallic", props, false);
                layer4NormalMode = FindProperty("_Layer4NormalMode", props, false);


                layer5Tex = FindProperty("_Layer5Tex", props, false);                
                layer5UVSet = FindProperty("_Layer5UVSet", props, false);
                layer5Normal = FindProperty("_Layer5Normal", props, false);
                layer5NormalScale = FindProperty("_Layer5NormalScale", props, false);
                layer5Color = FindProperty("_Layer5Color", props, false);
                layer5Smoothness = FindProperty("_Layer5Smoothness", props, false);
                layer5Metallic = FindProperty("_Layer5Metallic", props, false);
                layer5NormalMode = FindProperty("_Layer5NormalMode", props, false);


                layer6Tex = FindProperty("_Layer6Tex", props, false);                
                layer6UVSet = FindProperty("_Layer6UVSet", props, false);
                layer6Normal = FindProperty("_Layer6Normal", props, false);
                layer6NormalScale = FindProperty("_Layer6NormalScale", props, false);
                layer6Color = FindProperty("_Layer6Color", props, false);
                layer6Smoothness = FindProperty("_Layer6Smoothness", props, false);
                layer6Metallic = FindProperty("_Layer6Metallic", props, false);
                layer6NormalMode = FindProperty("_Layer6NormalMode", props, false);

                layer7Tex = FindProperty("_Layer7Tex", props, false);                
                layer7UVSet = FindProperty("_Layer7UVSet", props, false);
                layer7Normal = FindProperty("_Layer7Normal", props, false);
                layer7NormalScale = FindProperty("_Layer7NormalScale", props, false);
                layer7Color = FindProperty("_Layer7Color", props, false);
                layer7Smoothness = FindProperty("_Layer7Smoothness", props, false);
                layer7Metallic = FindProperty("_Layer7Metallic", props, false);
                layer7NormalMode = FindProperty("_Layer7NormalMode", props, false);
        #endregion    

    }

    public override void OnGUI(MaterialEditor materialEditor, MaterialProperty[] props)
    {
        FindProperties(props); // MaterialProperties can be animated so we do not cache them but fetch them every event to ensure animated values are updated correctly
        m_MaterialEditor = materialEditor;
        Material material = m_MaterialEditor.target as Material;

        // Make sure that needed setup (ie keywords/renderqueue) are set up if we're switching some existing
        // material to a standard shader.
        // Do this before any GUI code has been issued to prevent layout issues in subsequent GUILayout statements (case 780071)
        if (m_FirstTimeApply)
        {            
            int renderQueue = material.renderQueue;
            MaterialChanged(material);
            material.renderQueue = renderQueue;
            m_FirstTimeApply = false;
        }
        ShaderPropertiesGUI(material);
        // GUILayout.EndVertical();
        MaterialChanged(material);
    }

    public void ShaderPropertiesGUI(Material material)
    {
        // Use default labelWidth
        EditorGUIUtility.labelWidth = 0f;
        

        // Detect any changes to the material
        EditorGUI.BeginChangeCheck();
        {
            // GUILayout.Space(8f);
            // GUILayout.Space(4f);
            
            // Primary props
            GUILayout.Label(Styles.primaryMapsText, EditorStyles.boldLabel);
            DoTextureArea(material);
            GUILayout.Label(Styles.otherText, EditorStyles.boldLabel);
            DoFloatArea(material);
            GUILayout.Label(Styles.colorText, EditorStyles.boldLabel);
            DoColorArea(material);
            GUILayout.Label(Styles.layerText, EditorStyles.boldLabel);
            DoLayerArea(material);
            
        }
       
        if (EditorGUI.EndChangeCheck())
        {           
            SetMaterialKeywords(material);
        }

        EditorGUILayout.Space();                
        
    }

    void DoTextureArea(Material material)
    {        
        m_MaterialEditor.TexturePropertySingleLine(Styles.albedoText, albedoMap, _BaseColor);
        m_MaterialEditor.TexturePropertySingleLine(Styles.specularText, specularTex);
        m_MaterialEditor.TexturePropertySingleLine(Styles.bumpMapText,  bumpMap);
        m_MaterialEditor.TexturePropertySingleLine(Styles.lookupText, lutTex);        
    }


    void DoFloatArea(Material material)
    {
       m_MaterialEditor.ShaderProperty(fresnel, Styles.fresnelText.text);
       m_MaterialEditor.ShaderProperty(shadowIntensity, Styles.shadowIntensityText.text);
       m_MaterialEditor.ShaderProperty(extraShadeRange, Styles.extraShadeRangeText.text);
    }

    void DoColorArea(Material material)
    {
        m_MaterialEditor.ShaderProperty(_ShadowColor, Styles.shadowColorText.text);
        m_MaterialEditor.ShaderProperty(_ShadeColor, Styles.shadeColorText.text);
        m_MaterialEditor.ShaderProperty(_SpecularColor, Styles.specularColorText.text);
        m_MaterialEditor.ShaderProperty(_NN4AmbientTint, Styles.NN4AmbientTintText.text);        
    }
    // void TextureLayout(GUIContent content, params MaterialProperty[] property)
    // {
    //     GUILayout.BeginHorizontal();        
    //     GUILayout.Space(10);
        
    //         m_MaterialEditor.TexturePropertySingleLine(content, property);
    //     GUILayout.EndHorizontal();
    // }
    void TextureLayout(GUIContent content, MaterialProperty propert1, MaterialProperty propert2 = null)
    {
        GUILayout.BeginHorizontal();        
        GUILayout.Space(10);
        if (propert2 != null)
            m_MaterialEditor.TexturePropertySingleLine(content, propert1,propert2);
        else
            m_MaterialEditor.TexturePropertySingleLine(content, propert1);
        GUILayout.EndHorizontal();
    }
    void TextureLayout0(GUIContent content, MaterialProperty propert1, MaterialProperty propert2 = null)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Space(10);
        if (propert2 != null)
            m_MaterialEditor.TexturePropertySingleLine(content, propert1, propert2);
        else
            m_MaterialEditor.TexturePropertySingleLine(content, propert1);
        GUILayout.EndHorizontal();
    }
    
    void DoLayerArea(Material material)
    {
        if (layer0Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer0TexText, layer0Tex);
            TextureLayout0( Styles.layer0NormalText, layer0Normal, layer0NormalScale);
            if (material.GetTexture("_Layer0Tex") || material.GetTexture("_Layer0Normal"))
            {         
                if (material.GetTexture("_Layer0Tex"))
                {                                                                                                
                    m_MaterialEditor.TextureScaleOffsetProperty(layer0Tex);
                    m_MaterialEditor.ShaderProperty(layer0Color, Styles.layer0ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer0Normal);                                                                                               
                m_MaterialEditor.ShaderProperty(layer0Smoothness, Styles.layer0SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer0Metallic, Styles.layer0MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer0UVSet, Styles.layer0UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer0NormalMode, Styles.layer0NormalModeText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer0Normal", null);                    
            }
        }

        if (layer1Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer1TexText, layer1Tex);
            TextureLayout0( Styles.layer1NormalText, layer1Normal, layer1NormalScale);
            if (material.GetTexture("_Layer1Tex") || material.GetTexture("_Layer1Normal"))
            {                                        
                if (material.GetTexture("_Layer1Tex"))
                {                                                
                    m_MaterialEditor.TextureScaleOffsetProperty(layer1Tex);
                    m_MaterialEditor.ShaderProperty(layer1Color, Styles.layer1ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer1Normal);
                
                m_MaterialEditor.ShaderProperty(layer1Smoothness, Styles.layer1SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer1Metallic, Styles.layer1MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer1UVSet, Styles.layer1UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer1NormalMode, Styles.layer1NormalModeText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer1Cutoff, Styles.layer1CutoffText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer1Normal", null);                    
            }
        }

        if (layer2Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer2TexText, layer2Tex);
            TextureLayout0( Styles.layer2NormalText, layer2Normal, layer2NormalScale);
            if (material.GetTexture("_Layer2Tex") || material.GetTexture("_Layer2Normal"))
            {         
                if (material.GetTexture("_Layer2Tex"))
                {                                                                                                
                    m_MaterialEditor.TextureScaleOffsetProperty(layer2Tex);
                    m_MaterialEditor.ShaderProperty(layer2Color, Styles.layer2ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer2Normal);                                                                           
                
                m_MaterialEditor.ShaderProperty(layer2Smoothness, Styles.layer2SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer2Metallic, Styles.layer2MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer2UVSet, Styles.layer2UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer2NormalMode, Styles.layer2NormalModeText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer2Normal", null);                    
            }
        }
        if (layer3Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer3TexText, layer3Tex);
            TextureLayout0( Styles.layer3NormalText, layer3Normal, layer3NormalScale);
            if (material.GetTexture("_Layer3Tex") || material.GetTexture("_Layer3Normal"))
            {         
                if (material.GetTexture("_Layer3Tex"))
                {                                                                        
                    m_MaterialEditor.TextureScaleOffsetProperty(layer3Tex);
                    m_MaterialEditor.ShaderProperty(layer3Color, Styles.layer3ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer3Normal);                                                                           
                
                m_MaterialEditor.ShaderProperty(layer3Smoothness, Styles.layer3SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer3Metallic, Styles.layer3MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer3UVSet, Styles.layer3UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer3NormalMode, Styles.layer3NormalModeText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer3Normal", null);                    
            }
        }

        

        if (layer4Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer4TexText, layer4Tex);
            TextureLayout0( Styles.layer4NormalText, layer4Normal, layer4NormalScale);
            if (material.GetTexture("_Layer4Tex") || material.GetTexture("_Layer4Normal"))
            {         
                if (material.GetTexture("_Layer4Tex"))
                {                                                                        
                    m_MaterialEditor.TextureScaleOffsetProperty(layer4Tex);
                    m_MaterialEditor.ShaderProperty(layer4Color, Styles.layer4ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer4Normal);                                                                           
                
                m_MaterialEditor.ShaderProperty(layer4Smoothness, Styles.layer4SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer4Metallic, Styles.layer4MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer4UVSet, Styles.layer4UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer4NormalMode, Styles.layer4NormalModeText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer4Normal", null);                    
            }
        }


        if (layer5Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer5TexText, layer5Tex);
            TextureLayout0( Styles.layer5NormalText, layer5Normal, layer5NormalScale);
            if (material.GetTexture("_Layer5Tex") || material.GetTexture("_Layer5Normal"))
            {         
                if (material.GetTexture("_Layer5Tex"))
                {                                                                        
                    m_MaterialEditor.TextureScaleOffsetProperty(layer5Tex);
                    m_MaterialEditor.ShaderProperty(layer5Color, Styles.layer5ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer5Normal);                                                                           
                
                m_MaterialEditor.ShaderProperty(layer5Smoothness, Styles.layer5SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer5Metallic, Styles.layer5MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer5UVSet, Styles.layer5UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer5NormalMode, Styles.layer5NormalModeText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer5Normal", null);                    
            }
        }

        if (layer6Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer6TexText, layer6Tex);
            TextureLayout0( Styles.layer6NormalText, layer6Normal, layer6NormalScale);
            if (material.GetTexture("_Layer6Tex") || material.GetTexture("_Layer6Normal"))
            {         
                if (material.GetTexture("_Layer6Tex"))
                {                                                                        
                    m_MaterialEditor.TextureScaleOffsetProperty(layer6Tex);
                    m_MaterialEditor.ShaderProperty(layer6Color, Styles.layer6ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer6Normal);                                                                           
                
                m_MaterialEditor.ShaderProperty(layer6Smoothness, Styles.layer6SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer6Metallic, Styles.layer6MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer6UVSet, Styles.layer6UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer6NormalMode, Styles.layer6NormalModeText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer6Normal", null);                    
            }
        }


            if (layer7Tex != null)
        {
            m_MaterialEditor.TexturePropertySingleLine(Styles.layer7TexText, layer7Tex);
            TextureLayout0( Styles.layer7NormalText, layer7Normal, layer7NormalScale);
            if (material.GetTexture("_Layer7Tex") || material.GetTexture("_Layer7Normal"))
            {         
                if (material.GetTexture("_Layer7Tex"))
                {                                                                        
                    m_MaterialEditor.TextureScaleOffsetProperty(layer7Tex);
                    m_MaterialEditor.ShaderProperty(layer7Color, Styles.layer7ColorText.text, 2);
                }
                else
                    m_MaterialEditor.TextureScaleOffsetProperty(layer7Normal);                                                                           
                
                m_MaterialEditor.ShaderProperty(layer7Smoothness, Styles.layer7SmoothnessText.text, 2);
                // m_MaterialEditor.ShaderProperty(layer7Metallic, Styles.layer7MetallicText.text, 2);
                m_MaterialEditor.ShaderProperty(layer7UVSet, Styles.layer7UVSetText.text, 2);
                m_MaterialEditor.ShaderProperty(layer7NormalMode, Styles.layer7NormalModeText.text, 2);
            }
            else
            {
                material.SetTexture("_Layer7Normal", null);                    
            }
        }
        
    }
 
    static void SetMaterialKeywords(Material material)
    {        
        // if (material.HasProperty("_NormalMapBase"))
        // SetKeyword(material, "_NORMALMAP_BASE", material.GetTexture("_NormalMapBase"));

            if (material.HasProperty("_Layer0Tex"))
            SetKeyword(material, "_LAYER_0_TEX", material.GetTexture("_Layer0Tex"));
            if (material.HasProperty("_Layer1Normal"))
            SetKeyword(material, "_LAYER_0_NORMAL", material.GetTexture("_Layer0Normal"));            

            if (material.HasProperty("_Layer1Tex"))
            SetKeyword(material, "_LAYER_1_TEX", material.GetTexture("_Layer1Tex"));
            if (material.HasProperty("_Layer1Normal"))
            SetKeyword(material, "_LAYER_1_NORMAL", material.GetTexture("_Layer1Normal"));
            

            if (material.HasProperty("_Layer2Tex"))
            SetKeyword(material, "_LAYER_2_TEX", material.GetTexture("_Layer2Tex"));
            if (material.HasProperty("_Layer2Normal"))
            SetKeyword(material, "_LAYER_2_NORMAL", material.GetTexture("_Layer2Normal"));
            

            if (material.HasProperty("_Layer3Tex"))
            SetKeyword(material, "_LAYER_3_TEX", material.GetTexture("_Layer3Tex"));
            if (material.HasProperty("_Layer3Normal"))
            SetKeyword(material, "_LAYER_3_NORMAL", material.GetTexture("_Layer3Normal"));
            

            if (material.HasProperty("_Layer4Tex"))
            SetKeyword(material, "_LAYER_4_TEX", material.GetTexture("_Layer4Tex"));
            if (material.HasProperty("_Layer4Normal"))
            SetKeyword(material, "_LAYER_4_NORMAL", material.GetTexture("_Layer4Normal"));
            

            if (material.HasProperty("_Layer5Tex"))
            SetKeyword(material, "_LAYER_5_TEX", material.GetTexture("_Layer5Tex"));
            if (material.HasProperty("_Layer5Normal"))
            SetKeyword(material, "_LAYER_5_NORMAL", material.GetTexture("_Layer5Normal"));
            

            if (material.HasProperty("_Layer6Tex"))
            SetKeyword(material, "_LAYER_6_TEX", material.GetTexture("_Layer6Tex"));
            if (material.HasProperty("_Layer6Normal"))
            SetKeyword(material, "_LAYER_6_NORMAL", material.GetTexture("_Layer6Normal"));


            if (material.HasProperty("_Layer7Tex"))
            SetKeyword(material, "_LAYER_7_TEX", material.GetTexture("_Layer7Tex"));
            if (material.HasProperty("_Layer7Normal"))
            SetKeyword(material, "_LAYER_7_NORMAL", material.GetTexture("_Layer7Normal"));

            

            // key for layer uv set
            SetKeyword(material, "_Layer0UVSet_0", material.GetFloat("_Layer0UVSet") == 0);
            SetKeyword(material, "_Layer0UVSet_1", material.GetFloat("_Layer0UVSet") == 1);
            SetKeyword(material, "_Layer0UVSet_2", material.GetFloat("_Layer0UVSet") == 2);
            SetKeyword(material, "_Layer0UVSet_3", material.GetFloat("_Layer0UVSet") == 3);

            SetKeyword(material, "_Layer1UVSet_0", material.GetFloat("_Layer1UVSet") == 0);
            SetKeyword(material, "_Layer1UVSet_1", material.GetFloat("_Layer1UVSet") == 1);
            SetKeyword(material, "_Layer1UVSet_2", material.GetFloat("_Layer1UVSet") == 2);            
            SetKeyword(material, "_Layer1UVSet_3", material.GetFloat("_Layer1UVSet") == 3);            

            SetKeyword(material, "_Layer2UVSet_0", material.GetFloat("_Layer2UVSet") == 0);
            SetKeyword(material, "_Layer2UVSet_1", material.GetFloat("_Layer2UVSet") == 1);
            SetKeyword(material, "_Layer2UVSet_2", material.GetFloat("_Layer2UVSet") == 2);            
            SetKeyword(material, "_Layer2UVSet_3", material.GetFloat("_Layer2UVSet") == 3);            

            SetKeyword(material, "_Layer3UVSet_0", material.GetFloat("_Layer3UVSet") == 0);
            SetKeyword(material, "_Layer3UVSet_1", material.GetFloat("_Layer3UVSet") == 1);
            SetKeyword(material, "_Layer3UVSet_2", material.GetFloat("_Layer3UVSet") == 2);            
            SetKeyword(material, "_Layer3UVSet_3", material.GetFloat("_Layer3UVSet") == 3);            

            SetKeyword(material, "_Layer4UVSet_0", material.GetFloat("_Layer4UVSet") == 0);
            SetKeyword(material, "_Layer4UVSet_1", material.GetFloat("_Layer4UVSet") == 1);
            SetKeyword(material, "_Layer4UVSet_2", material.GetFloat("_Layer4UVSet") == 2);            
            SetKeyword(material, "_Layer4UVSet_3", material.GetFloat("_Layer4UVSet") == 3);            

            SetKeyword(material, "_Layer5UVSet_0", material.GetFloat("_Layer5UVSet") == 0);
            SetKeyword(material, "_Layer5UVSet_1", material.GetFloat("_Layer5UVSet") == 1);
            SetKeyword(material, "_Layer5UVSet_2", material.GetFloat("_Layer5UVSet") == 2);            
            SetKeyword(material, "_Layer5UVSet_3", material.GetFloat("_Layer5UVSet") == 3);            

            SetKeyword(material, "_Layer6UVSet_0", material.GetFloat("_Layer6UVSet") == 0);
            SetKeyword(material, "_Layer6UVSet_1", material.GetFloat("_Layer6UVSet") == 1);
            SetKeyword(material, "_Layer6UVSet_2", material.GetFloat("_Layer6UVSet") == 2);            
            SetKeyword(material, "_Layer6UVSet_3", material.GetFloat("_Layer6UVSet") == 3);            

            SetKeyword(material, "_Layer7UVSet_0", material.GetFloat("_Layer7UVSet") == 0);
            SetKeyword(material, "_Layer7UVSet_1", material.GetFloat("_Layer7UVSet") == 1);
            SetKeyword(material, "_Layer7UVSet_2", material.GetFloat("_Layer7UVSet") == 2);            
            SetKeyword(material, "_Layer7UVSet_3", material.GetFloat("_Layer7UVSet") == 3);            



            // Normal Blend Mode
            SetKeyword(material, "_Layer0NormalMode_0", material.GetFloat("_Layer0NormalMode") == 0);
            SetKeyword(material, "_Layer0NormalMode_1", material.GetFloat("_Layer0NormalMode") == 1);

            SetKeyword(material, "_Layer1NormalMode_0", material.GetFloat("_Layer1NormalMode") == 0);
            SetKeyword(material, "_Layer1NormalMode_1", material.GetFloat("_Layer1NormalMode") == 1);

            SetKeyword(material, "_Layer2NormalMode_0", material.GetFloat("_Layer2NormalMode") == 0);
            SetKeyword(material, "_Layer2NormalMode_1", material.GetFloat("_Layer2NormalMode") == 1);

            SetKeyword(material, "_Layer3NormalMode_0", material.GetFloat("_Layer3NormalMode") == 0);
            SetKeyword(material, "_Layer3NormalMode_1", material.GetFloat("_Layer3NormalMode") == 1);

            SetKeyword(material, "_Layer4NormalMode_0", material.GetFloat("_Layer4NormalMode") == 0);
            SetKeyword(material, "_Layer4NormalMode_1", material.GetFloat("_Layer4NormalMode") == 1);

            SetKeyword(material, "_Layer5NormalMode_0", material.GetFloat("_Layer5NormalMode") == 0);
            SetKeyword(material, "_Layer5NormalMode_1", material.GetFloat("_Layer5NormalMode") == 1);

            SetKeyword(material, "_Layer6NormalMode_0", material.GetFloat("_Layer6NormalMode") == 0);
            SetKeyword(material, "_Layer6NormalMode_1", material.GetFloat("_Layer6NormalMode") == 1);

            SetKeyword(material, "_Layer7NormalMode_0", material.GetFloat("_Layer7NormalMode") == 0);
            SetKeyword(material, "_Layer7NormalMode_1", material.GetFloat("_Layer7NormalMode") == 1);            

            // TODO:
            material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Geometry + 2;

    }

    
    static void MaterialChanged(Material material)
    {
        // SetupMaterialWithBlendMode(material, (BlendMode)material.GetFloat("_Mode"));

        SetMaterialKeywords(material);
    }

    static void SetKeyword(Material m, string keyword, bool state)
    {
        if (state)
            m.EnableKeyword(keyword);
        else
            m.DisableKeyword(keyword);
    }
}
