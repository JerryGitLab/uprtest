﻿Shader "Fur/Fur_Shader"
{
    Properties
    {     
        _Specular ("Specular", Color) = (1, 1, 1, 1)
        _Shininess ("Shininess[高光范围]", Range(0.01, 256.0)) = 8.0
        _FurColorTex ("FurColorTex[毛发]", 2D) = "white" { }
        _FurColor ("FurColor[毛发]", Color) = (1, 1, 1, 1)
        [NoScaleOffset]_MaskTex ("Mask[毛发遮罩]", 2D) = "white" { }     
        _LayerTex ("LayerTex[毛发噪声图]", 2D) = "white" { }                
        [NoScaleOffset]_LayerTexColor ("LayerTexColor[噪声颜色]", 2D) = "white" { }
        _FurLength ("Fur Length", Range(0.0, 1)) = 0.5
        _FurAlpha ("Fur Alpha", Range(0.0, 0.5)) = 0.1
        _FurDensity ("Fur Density", Range(0, 4)) = 0.11
        _FurThinness ("Fur Thinness", Range(0.01, 10)) = 1
        _FurShading ("Fur Shading", Range(0.0, 1)) = 0.25   
        _Gravity("Gravity Direction[重力方向]", Vector) = (0,-1,0,0)
		_GravityStrength("Gravity Strength", Range(0,1)) = 0.25
        _RimColor ("Rim Color", Color) = (0, 0, 0, 1)
        _RimPower ("Rim Power", Range(0.0, 8.0)) = 6.0
    }
    
    SubShader
    {
        Tags { "RenderType" = "Transparent" "IgnoreProjector" = "True" "Queue"="Transparent+50"}
        Cull Off
        ZWrite On
        Blend SrcAlpha OneMinusSrcAlpha
        
        HLSLINCLUDE
        float FURSTEP;
        float4 _Specular;//FurSpecularColor
        half _Shininess;

        // 毛皮固有色
        sampler2D _MainTex;
        half4 _MainTex_ST;

        // 毛发遮罩
        sampler2D _MaskTex;
        half4 _MaskTex_ST;

        // 毛发颜色
        sampler2D _FurColorTex;
        half4 _FurColorTex_ST;
        float4 _FurColor;


        sampler2D _LayerTex; //FurPattern
        half4 _LayerTex_ST;

        sampler2D _LayerTexColor;
        half4 _LayerTexColor_ST;

        float _FurLength;
        float _FurAlpha;

        float _FurDensity;
        float _FurThinness;
        float _FurShading; //FurShadow

        float4 _Gravity;
        float _GravityStrength;

        float4 _RimColor; //FurRimColor
        half _RimPower;
        half _RenderQueue;

        struct Attributes
        {
            float4 positionOS : POSITION;
            float4 normal : NORMAL;
            float4 texcoord : TEXCOORD0;               
        };

        struct Varyings
        {
            float4 positionCS: SV_POSITION;
            half4 uv: TEXCOORD0;
            float3 worldNormal: TEXCOORD1;
            float3 worldPos: TEXCOORD2;    
        };           
        ENDHLSL

        Pass
        {
            Tags{ "LightMode" = "FurRendererBase" "RenderType" = "Opaque" }
            HLSLPROGRAM
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"  
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityInput.hlsl"
            #pragma vertex vert_base           
            #pragma fragment frag_base

            Varyings vert_base(Attributes v)
            {
                Varyings o = (Varyings)0;
                o.positionCS = TransformObjectToHClip(v.positionOS);
                o.uv.xy = TRANSFORM_TEX(v.texcoord, _FurColorTex);
                o.worldNormal = TransformObjectToWorldNormal(v.normal);
                o.worldPos = mul(unity_ObjectToWorld, v.positionOS).xyz;
                return o;
            }

            float4 frag_base(Varyings i): SV_Target
            {               
                float3 worldNormal = normalize(i.worldNormal);
                float3 worldLight = normalize(_MainLightPosition.xyz);
                float3 worldView = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
                float3 worldHalf = normalize(worldView + worldLight);
                
                float3 albedo = _FurColor; // 表皮值包含固有色
                float3 ambient = _GlossyEnvironmentColor.xyz * albedo;
                float3 diffuse = _MainLightColor * albedo * saturate(dot(worldNormal, worldLight));
                float3 specular = _MainLightColor * _Specular.rgb * pow(saturate(dot(worldNormal, worldHalf)), _Shininess);
                float3 color = ambient + diffuse + specular;                
                return float4(color, 1.0);
            }
            ENDHLSL
        }
    
        Pass
        {
			Tags{ "LightMode" = "FurRendererLayer"}
            HLSLPROGRAM     
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityInput.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"       
            #pragma vertex vert_layer
            #pragma fragment frag_layer

            Varyings vert_layer(Attributes v)
            {
                Varyings o = (Varyings)0;
                half3 direction = lerp(v.normal, _Gravity * _GravityStrength + v.normal * (1 - _GravityStrength), FURSTEP);
                float3 P =  direction * _FurLength * FURSTEP;
                // add mask calculate. 
                o.uv.xy = TRANSFORM_TEX(v.texcoord, _FurColorTex); 
                o.uv.zw = TRANSFORM_TEX(v.texcoord, _LayerTex);

                float4 mask = tex2Dlod(_MaskTex,float4(o.uv.xy,0,0));    

                P =  v.positionOS.xyz + P * mask.r;
                
                o.worldNormal = TransformObjectToWorldNormal(v.normal);
                o.positionCS = TransformObjectToHClip(P);                                              
                o.worldPos = mul(unity_ObjectToWorld, v.positionOS).xyz;

                return o;
            }

            float4 frag_layer(Varyings i): SV_Target
            {
                float3 worldNormal = normalize(i.worldNormal);
                float3 worldLight = normalize(_MainLightPosition.xyz);
                float3 worldView = normalize(_WorldSpaceCameraPos.xyz - i.worldPos.xyz);
                float3 worldHalf = normalize(worldView + worldLight);
                float3 albedo = tex2D(_FurColorTex, i.uv.xy).rgb * _FurColor;
                albedo -= (pow(1 - FURSTEP, 3)) * _FurShading;             
                //边缘光
                half vdotn = 1.0 - saturate(dot(worldView, worldNormal));
                float3 rim = _RimColor.rgb * saturate( 1-pow( 1-vdotn, _RimPower));        
                float3 noise = tex2D(_LayerTex, i.uv.zw * _FurThinness).rgb;
                float3 layerColor = tex2D(_LayerTexColor, i.uv.zw * _FurThinness).rgb;
                float3 noiseColor = layerColor*noise;
                albedo = lerp(albedo,albedo*noiseColor,noise.r);
                float3 ambient = _GlossyEnvironmentColor.xyz * albedo;
                float3 diffuse = _MainLightColor * albedo * saturate(dot(worldNormal, worldLight));
                float3 specular = _MainLightColor * _Specular.rgb * pow(saturate(dot(worldNormal, worldHalf)), _Shininess);
                float3 color = ambient + diffuse + specular + rim;
                float alpha = saturate(noise - _FurAlpha- (FURSTEP * FURSTEP) * _FurDensity);
                return float4(color, alpha);
            }            
            ENDHLSL    
        }
    }
}