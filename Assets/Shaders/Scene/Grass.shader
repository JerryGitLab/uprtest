Shader "URP/Grass"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        [HDR]_MainColor("MainColor",Color) = (1,1,1,1)
        _Noise("Noise",2D) = "black"{}
        _WindControl("WindControl",vector) = (1,0,1,0.5)
        _WaveControl("WaveControl",vector) = (1,0,1,1)
        _Cutoff("Cutoff Value",Range(0,1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        ZWrite On
        LOD 100

        Pass
        {
            Tags{ "RenderPipeline"="UniversalRenderPipeline" "LightMode" = "UniversalForward" "RenderType" = "Opaque" }
            HLSLPROGRAM
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"  
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/UnityInput.hlsl"
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #pragma multi_compile_fog

            struct Attributes
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                float2 uv : TEXCOORD0;
                float4 worldPos : TEXCOORD1;
                float4 pos : SV_POSITION;
                float fogFactor : TEXCOORD2;
            };

            sampler2D _MainTex;
            sampler2D _Noise;
            half4 _MainColor;
            half4 _WindControl;
            half4 _WaveControl;
            half _Cutoff;

            Varyings vert (Attributes v)
            {
                Varyings o = (Varyings)0;
                UNITY_SETUP_INSTANCE_ID(v);
                float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                float2 samplePos = worldPos.xz / _WaveControl.w;
                samplePos += _Time.x * -_WaveControl.xz;
                half waveSample = tex2Dlod(_Noise, float4(samplePos, 0, 0)).r;
                worldPos.x += sin(waveSample * _WindControl.x) * _WaveControl.x * _WindControl.w * v.uv.y;
                worldPos.z += sin(waveSample * _WindControl.z) * _WaveControl.z * _WindControl.w * v.uv.y;
                o.worldPos = worldPos;
                o.pos = mul(UNITY_MATRIX_VP, worldPos);
                o.fogFactor = ComputeFogFactor(o.pos.z);
                o.uv = v.uv;
                return o;
            }

            float4 frag (Varyings i) : SV_Target
            {
                float4 mainColor = tex2D(_MainTex, i.uv);
                float3 col = mainColor.xyz * _MainColor.xyz;
                float alpha = mainColor.a;
                clip(alpha - _Cutoff); 
                col.rgb = MixFog(col.rgb, i.fogFactor);
                return float4(col,alpha);
            }
            ENDHLSL
        }
        UsePass "Universal Render Pipeline/Lit/ShadowCaster"
    }
}