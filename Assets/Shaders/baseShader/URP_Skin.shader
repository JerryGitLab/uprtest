Shader "CustomURP/Skin_URP"
{
    Properties
    {
        _MainTexBase ("Albedo", 2D) = "white" {}
        _MainTexBaseColor ("BaseColor",Color) = (1,1,1,1)
        _ShadowColor ("ShadowColor",Color) = (0.47,0.31,0.40,1)
        _ShadeColor ("ShadeColor",Color) = (0.91,0.636,0.636,1)
        _SpecularColorBase ("高光颜色",Color) = (1,1,1,1)
        _SpecularTex ("光滑度(R) 曲率(G) 附加阴影(B)", 2D) = "white" {} 
        _NormalMapBase ("Normal Map", 2D) = "bump" {}        
        // _NormalMapScale("_NormalMapScale", Range(-2, 2)) = 1
        _LUTTex ("BRDF Lookup(RGB)", 2D) = "gray" {}
        _Fresnel ("菲涅尔",Range(0, 1)) = 0.124
        
        _ShadowIntensity ("阴影浓度",Range(0, 3)) = 1.48
        _ExtraShadeRange ("附加阴影范围",Range(0, 3)) = 0.6

        // 外部传入
        _NN4AmbientTint ("NN4AmbientTint[环境光]",Color) = (1,1,1,1)
        // _NN4Char_LightColor1 ("NN4Char_LightColor1",Color) = (0.31,0.31,0.31,1)
        // [HDR]_NN4Char_LightColor2 ("NN4Char_LightColor2",Color) = (5.41,5.41,5.41,1)
        
        // _NN4Char_LightDir1 ("NN4Char_LightDir1",Vector) = (-0.27,0.45,0.85,1)
        // _NN4Char_LightDir2 ("NN4Char_LightDir2",Vector) = (0.72,0.369,-0.582,1)        
        
        
        _DeptheBias ("DeptheBias",float) = 0                        

    // Layer
        _Layer0Tex("Layer0Tex", 2D) = "black"{}		
        _Layer0Normal("Layer0Normal", 2D) = "black"{}
        _Layer0NormalScale("Layer0NormalScale", float) = 1        
        [HDR]_Layer0Color("Layer1Color", Color) = (1,1,1,1)
        _Layer0Smoothness("Layer0Smoothness", Range(-1, 1)) = 0
		_Layer0Metallic("Layer0Metallic", Range(-1, 1)) = 0
        [Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer0UVSet("Layer0UVSet", float) = 0
        [Enum(Blend,0,Cover,1)] _Layer0NormalMode("Layer0NormalMode", float) = 0

		_Layer1Tex("Layer1Tex", 2D) = "black"{}		
		_Layer1Normal("Layer1Normal", 2D) = "bump"{}
		_Layer1NormalScale("Layer1NormalScale", float) = 1
		[HDR]_Layer1Color("Layer1Color", Color) = (1,1,1,1)
		_Layer1Smoothness("Layer1Smoothness", Range(-1, 1)) = 0
		_Layer1Metallic("Layer1Metallic", Range(-1, 1)) = 0
		[Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer1UVSet("Layer1UVSet", float) = 0        
        [Enum(Blend,0,Cover,1)] _Layer1NormalMode("Layer1NormalMode", float) = 0
        _Layer1Cutoff("Layer1Alpha Cutoff", Range(0.0, 1.0)) = 0.5

        _Layer2Tex("Layer2Tex", 2D) = "black"{}		
		_Layer2Normal("Layer2Normal", 2D) = "bump"{}
		_Layer2NormalScale("Layer2NormalScale", float) = 1
		[HDR]_Layer2Color("Layer2Color", Color) = (1,1,1,1)
		_Layer2Smoothness("Layer2Smoothness", Range(-1, 1)) = 0
		_Layer2Metallic("Layer2Metallic", Range(-1, 1)) = 0
		[Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer2UVSet("Layer2UVSet", float) = 0
        [Enum(Blend,0,Cover,1)] _Layer2NormalMode("Layer2NormalMode", float) = 0

        _Layer3Tex("Layer3Tex", 2D) = "black"{}		
		_Layer3Normal("Layer3Normal", 2D) = "bump"{}
		_Layer3NormalScale("Layer3NormalScale", float) = 1
		[HDR]_Layer3Color("Layer3Color", Color) = (1,1,1,1)
		_Layer3Smoothness("Layer3Smoothness", Range(-1, 1)) = 0
		_Layer3Metallic("Layer3Metallic", Range(-1, 1)) = 0
		[Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer3UVSet("Layer3UVSet", float) = 0
        [Enum(Blend,0,Cover,1)] _Layer3NormalMode("Layer3NormalMode", float) = 0

        _Layer4Tex("Layer4Tex", 2D) = "black"{}		
		_Layer4Normal("Layer4Normal", 2D) = "bump"{}
		_Layer4NormalScale("Layer4NormalScale", float) = 1
		[HDR]_Layer4Color("Layer4Color", Color) = (1,1,1,1)
		_Layer4Smoothness("Layer4Smoothness", Range(-1, 1)) = 0
		_Layer4Metallic("Layer4Metallic", Range(-1, 1)) = 0
		[Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer4UVSet("Layer4UVSet", float) = 0
        [Enum(Blend,0,Cover,1)] _Layer4NormalMode("Layer4NormalMode", float) = 0

        _Layer5Tex("Layer5Tex", 2D) = "black"{}		
		_Layer5Normal("Layer5Normal", 2D) = "bump"{}
		_Layer5NormalScale("Layer5NormalScale", float) = 1
		[HDR]_Layer5Color("Layer5Color", Color) = (1,1,1,1)
		_Layer5Smoothness("Layer5Smoothness", Range(-1, 1)) = 0
		_Layer5Metallic("Layer5Metallic", Range(-1, 1)) = 0
		[Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer5UVSet("Layer5UVSet", float) = 0
        [Enum(Blend,0,Cover,1)] _Layer5NormalMode("Layer5NormalMode", float) = 0

        _Layer6Tex("Layer6Tex", 2D) = "black"{}		
		_Layer6Normal("Layer6Normal", 2D) = "bump"{}
		_Layer6NormalScale("Layer6NormalScale", float) = 1
		[HDR]_Layer6Color("Layer6Color", Color) = (1,1,1,1)
		_Layer6Smoothness("Layer6Smoothness", Range(-1, 1)) = 0
		_Layer6Metallic("Layer6Metallic", Range(-1, 1)) = 0
		[Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer6UVSet("Layer6UVSet", float) = 0
        [Enum(Blend,0,Cover,1)] _Layer6NormalMode("Layer6NormalMode", float) = 0

        _Layer7Tex("Layer7Tex", 2D) = "black"{}		
		_Layer7Normal("Layer7Normal", 2D) = "bump"{}
		_Layer7NormalScale("Layer7NormalScale", float) = 1
		[HDR]_Layer7Color("Layer7Color", Color) = (1,1,1,1)
		_Layer7Smoothness("Layer7Smoothness", Range(-1, 1)) = 0
		_Layer7Metallic("Layer7Metallic", Range(-1, 1)) = 0
		[Enum(UV0,0, UV1,1, UV2,2, UV3,3)] _Layer7UVSet("Layer7UVSet", float) = 0      
        [Enum(Blend,0,Cover,1)] _Layer7NormalMode("Layer7NormalMode", float) = 0
    // Layer

        
    }
    SubShader
    {
        
        // Tags { "LightMode"="ForwardBase" "Classification" = "Skin"  "QUEUE" = "Geometry" "RenderType" = "NN4Skin" }          
        // Tags { "LightMode"="ForwardBase" "RenderPipeline" ="UniversalPipeline"}      
        // Tags { "RenderType"="Transparent" "Queue" = "Transparent-450" "RenderPipeline"="UniversalPipeline"}
        // "RenderType"="Opaque"
        Tags { "RenderType"="Opaque" "Queue" = "Geometry+2" "RenderPipeline"="UniversalPipeline"}
        Pass
        {
            Tags { "LightMode"="UniversalForward"  }               
            ZWrite On            
            HLSLPROGRAM
            
            #pragma target 3.0
            
            // #pragma shader_feature_local _NORMALMAP_BASE
            
            #pragma shader_feature_local _LAYER_0_TEX
            #pragma shader_feature_local _LAYER_1_TEX
            #pragma shader_feature_local _LAYER_2_TEX
			#pragma shader_feature_local _LAYER_3_TEX
			#pragma shader_feature_local _LAYER_4_TEX
			#pragma shader_feature_local _LAYER_5_TEX
			#pragma shader_feature_local _LAYER_6_TEX
			#pragma shader_feature_local _LAYER_7_TEX		

            #pragma shader_feature_local _LAYER_0_NORMAL
            #pragma shader_feature_local _LAYER_1_NORMAL
            #pragma shader_feature_local _LAYER_2_NORMAL
			#pragma shader_feature_local _LAYER_3_NORMAL
			#pragma shader_feature_local _LAYER_4_NORMAL
			#pragma shader_feature_local _LAYER_5_NORMAL
			#pragma shader_feature_local _LAYER_6_NORMAL			
			#pragma shader_feature_local _LAYER_7_NORMAL			

            #pragma shader_feature_local _Layer0UVSet_0 _Layer0UVSet_1 _Layer0UVSet_2 _Layer0UVSet_3
			#pragma shader_feature_local _Layer1UVSet_0 _Layer1UVSet_1 _Layer1UVSet_2 _Layer1UVSet_3
			#pragma shader_feature_local _Layer2UVSet_0 _Layer2UVSet_1 _Layer2UVSet_2 _Layer2UVSet_3
			#pragma shader_feature_local _Layer3UVSet_0 _Layer3UVSet_1 _Layer3UVSet_2 _Layer3UVSet_3
			#pragma shader_feature_local _Layer4UVSet_0 _Layer4UVSet_1 _Layer4UVSet_2 _Layer4UVSet_3
			#pragma shader_feature_local _Layer5UVSet_0 _Layer5UVSet_1 _Layer5UVSet_2 _Layer5UVSet_3
			#pragma shader_feature_local _Layer6UVSet_0 _Layer6UVSet_1 _Layer6UVSet_2 _Layer6UVSet_3
			#pragma shader_feature_local _Layer7UVSet_0 _Layer7UVSet_1 _Layer7UVSet_2 _Layer7UVSet_3

            #pragma shader_feature_local _Layer0NormalMode_0 _Layer0NormalMode_1
            #pragma shader_feature_local _Layer1NormalMode_0 _Layer1NormalMode_1
            #pragma shader_feature_local _Layer2NormalMode_0 _Layer2NormalMode_1
            #pragma shader_feature_local _Layer3NormalMode_0 _Layer3NormalMode_1
            #pragma shader_feature_local _Layer4NormalMode_0 _Layer4NormalMode_1
            #pragma shader_feature_local _Layer5NormalMode_0 _Layer5NormalMode_1
            #pragma shader_feature_local _Layer6NormalMode_0 _Layer6NormalMode_1
            #pragma shader_feature_local _Layer7NormalMode_0 _Layer7NormalMode_1
            
            #pragma vertex vert
            #pragma fragment frag                    
                        
            #define UNITY_INV_PI 1/PI
            #include "Packages/com.unity.render-pipelines.universal/Shaders/LitInput.hlsl"            
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
            // #include "Tools_URP.cginc"
            
            struct appdata
            {
                float4 vertex  : POSITION;                
                float4 tangent : TANGENT;
                float3 normal  : NORMAL;
                float2 uv0      : TEXCOORD0;
                float2 uv1      : TEXCOORD1;
                float2 uv2      : TEXCOORD2;
                float2 uv3      : TEXCOORD3;

            };

            struct v2f
            {                
                float4 vertex : SV_POSITION;
                float4 uv     : TEXCOORD0;
		        float4 TtoW0  : TEXCOORD1;  
			    float4 TtoW1  : TEXCOORD2;  
			    float4 TtoW2  : TEXCOORD3;
			    float3 shlight: TEXCOORD4;
			    float4 detailUV: TEXCOORD5;
			    float2 depthUV: TEXCOORD6;
			    	    
            };                                          
            float4 _ShadowColor;
            float4 _ShadeColor;
            float4 _NN4AmbientTint;
            float4 _NN4Char_LightColor1;
            float4 _NN4Char_LightColor2;
            float4 _NN4Char_LightDir1;
            float4 _NN4Char_LightDir2;
            float4 _SpecularColorBase;
            float4 _MainTexBaseColor;
            
            
            float _Fresnel;
            float _ShadowIntensity;
            float _ExtraShadeRange;
            float _DeptheBias;            
            

// // ------Added Layer- TODO: need to decrease sample numbles----------------    

// // ------Added Layer-----------------    
            
            TEXTURE2D(_MainTexBase);    SAMPLER(sampler_MainTexBase);
            TEXTURE2D(posm_ShadowMaskSkin);    SAMPLER(sampler_posm_ShadowMaskSkin);
            TEXTURE2D(_NormalMapBase);    SAMPLER(sampler_NormalMapBase);
            TEXTURE2D(_LUTTex);    SAMPLER(sampler_LUTTex);
            TEXTURE2D(_SpecularTex);    SAMPLER(sampler_SpecularTex);
            #include "Tools_URP.cginc"

            half3 FresnelLerpNN2 (half _Fresnel, half ndotv)
            {
                half fresnel = min(_Fresnel*2,1.0) - _Fresnel;
                ndotv = 1- ndotv;
                fresnel = pow(ndotv,4)*fresnel + _Fresnel;
                return half3(fresnel,fresnel,fresnel);
            }

            half3 FresnelLerpNN (half3 F0, half cosA)
            {
                //  ndov^4 * __Fresnel + __Fresnel;  (2*__Fresnel < 1)
                half t = Pow4 (1 - cosA);
                return (1+t) * F0;                
            }
            
            float GGXTerm(float NdotH, float roughness)
            {
                float a2 = roughness * roughness;
                float d = (NdotH * a2 - NdotH) * NdotH + 1.0f; // 2 mad
                return UNITY_INV_PI * a2 / (d * d + 1e-7f); // This function is not intended to be running on Mobile,
                                                        // therefore epsilon is smaller than what can be represented by half
            }

            v2f vert (appdata v)
            {
                v2f o;                                                                                               
                float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                float4 clipPos = TransformObjectToHClip(v.vertex.xyz);
                // clipPos.z = -clipPos.z * _DeptheBias*0.1 + clipPos.z;
                clipPos.z = clipPos.z;
                o.vertex = clipPos;                
                o.depthUV = (o.vertex.xy / o.vertex.w)*0.5+0.5; // zw for depth
                o.uv.xy = v.uv0;                
                o.uv.zw = v.uv1;
                o.detailUV.xy = v.uv2;
                o.detailUV.zw = v.uv3;
                float3 worldNormal = TransformObjectToWorldNormal(v.normal);  
				float3 worldTangent = TransformObjectToWorldDir(v.tangent.xyz);                
				float3 worldBinormal = cross(worldNormal, worldTangent) * v.tangent.w;
                worldBinormal = normalize(worldBinormal);

                o.TtoW0 = float4(worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x);  
				o.TtoW1 = float4(worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y);  
				o.TtoW2 = float4(worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z);  

                float3 shlight = SampleSHVertex(worldNormal);
                o.shlight = shlight;

      
                return o;
            }            
            float4 frag (v2f i) : SV_Target
            {
            #if !defined(UNITY_NO_LINEAR_COLORSPACE)
                _NN4Char_LightColor1 = pow(_NN4Char_LightColor1,0.45);
                i.shlight = pow(i.shlight,0.45);                  
            #endif    
                float3 baseColor = SAMPLE_TEXTURE2D(_MainTexBase, sampler_MainTexBase,i.uv).xyz;

                float3 _BaseColor = baseColor*baseColor*_MainTexBaseColor;                
                float3 worldPos = float3(i.TtoW0.w, i.TtoW1.w, i.TtoW2.w);
                float3 worldViewDir = normalize(_WorldSpaceCameraPos.xyz - worldPos);

                float3 normalTS = SAMPLE_TEXTURE2D(_NormalMapBase, sampler_NormalMapBase,i.uv.xy);
                normalTS.xy = normalTS.xy*2 -1;
                float _mask = normalTS.z;
                normalTS.z = sqrt(1 - saturate(dot(normalTS.xy, normalTS.xy)));				
                                                                                  
                float4 funcTex = SAMPLE_TEXTURE2D(_SpecularTex,sampler_SpecularTex, i.uv.xy);
                float smoothness = funcTex.x;
                F_SurfaceData surfaceData;
                InitializeSkinSurfaceData(i.uv,i.detailUV,_BaseColor, normalTS, smoothness, surfaceData);

// ------------------
                half3 worldNormal = surfaceData.normalTS;
                worldNormal = normalize(float3(dot(i.TtoW0.xyz, worldNormal), dot(i.TtoW1.xyz, worldNormal), dot(i.TtoW2.xyz, worldNormal)));
                _BaseColor = surfaceData.albedo;
                // return half4(_BaseColor,1);
                smoothness = surfaceData.smoothness;
                // metallic = surfaceData.metallic; // Todo: _SpecularColorBase
// ------------------

                float perceptualRoughness = 1 - smoothness;
                half roughness = max(perceptualRoughness*perceptualRoughness,0.1); // N func used Roughness.

                float3 R = reflect(-worldViewDir, worldNormal); 
                float ndotv = max(dot(worldNormal,worldViewDir), 0);
                      
                float3 IndirectSpecular = GlossyEnvironmentReflection(R, perceptualRoughness, 1);
                IndirectSpecular = IndirectSpecular * _NN4AmbientTint;

                float3 fresnel = FresnelLerpNN2(_Fresnel,ndotv);
                IndirectSpecular = IndirectSpecular * fresnel * i.shlight;                
                
                float D = GGXTerm(ndotv,roughness);
                
                float F = lerp(_Fresnel,1,1-max(ndotv,0));
                
                float3 SpecularColor = D*F*_NN4Char_LightColor1.xyz + IndirectSpecular;
                SpecularColor = SpecularColor *_SpecularColorBase;                

                float ndotl_1 = dot(worldNormal,_NN4Char_LightDir1);
                float ndotl_2 = dot(worldNormal,_NN4Char_LightDir2);                
                ndotl_2 = lerp(0.33,1,ndotl_2);
                ndotl_2 = max(ndotl_2,0);
                ndotl_2 = pow(ndotl_2,4);

                
                float3 _LightColor2 = ndotl_2 * _NN4Char_LightColor2 * _BaseColor;
                _LightColor2 = _LightColor2*_mask;
                _LightColor2 = _LightColor2 / (_LightColor2+1);

                float depth = SAMPLE_TEXTURE2D(posm_ShadowMaskSkin,sampler_posm_ShadowMaskSkin, i.depthUV);                                   
                float depthTemp = saturate(_ShadowIntensity*(depth - 1) + 1);
				depth = depthTemp - 1;				
                ndotl_1 = depth/4 + ndotl_1;
                ndotl_1 = ndotl_1 * 0.5 + 0.5;                                 
                float3 _LUTColor = SAMPLE_TEXTURE2D(_LUTTex,sampler_LUTTex,float2(ndotl_1, funcTex.y));
                
                float3 _FinalShadowColor = _LUTColor * lerp(_ShadowColor.xyz, half3(1,1,1), depthTemp);                

                _FinalShadowColor = _FinalShadowColor * _NN4Char_LightColor1 + i.shlight;                
                _FinalShadowColor = _BaseColor * _FinalShadowColor + _LightColor2;
                            
                float ndotVRange =  min(ndotv / _ExtraShadeRange,1) - 1;
                funcTex.z = 1 - funcTex.z;
                ndotVRange = ndotVRange * funcTex.z + 1; 
                float3 _lerpShadeColor = lerp(_ShadeColor,1,ndotVRange);
                float3 finalColor = _FinalShadowColor * _lerpShadeColor + SpecularColor;
                
                return float4(finalColor, 1);

            }           
            ENDHLSL            
        }        
        Pass
        {
            Name "NNShadowMap"
            Tags{"LightMode" = "NNShadowMap"}

            // ZWrite On
            ColorMask 0
            Cull[_Cull]

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard srp library
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 3.5

            #pragma vertex TestVertex
            #pragma fragment TestFragment
            
            struct Attributes
            {
                float4 position     : POSITION;                                
            };

            struct Varyings
            {                
                float4 positionCS   : SV_POSITION;                
            };

            Varyings TestVertex(Attributes input)
            {
                Varyings output = (Varyings)0;              
                output.positionCS = half4(input.position.xyz,1);
                return output;
            }

            half4 TestFragment(Varyings input) : SV_TARGET
            {        
                return 0;
            }

            ENDHLSL
        }
    }
    CustomEditor "SkinShaderGUI_Layer"
}
