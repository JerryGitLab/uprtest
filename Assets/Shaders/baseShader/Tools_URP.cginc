#ifndef FUN_SKINTOOL_URP_INCLUDED
#define FUN_SKINTOOL_URP_INCLUDED

#define TEXTURE2D_SAMPLER(tex) TEXTURE2D(tex); SAMPLER(sampler##tex);
#define SAMPLE_TEXTURE2D_Default(textureName, uv)  SAMPLE_TEXTURE2D(textureName,sampler##textureName,uv)

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/SurfaceInput.hlsl"


struct FragmentCommonData
{
    half3 diffColor, specColor;
    // Note: smoothness & oneMinusReflectivity for optimization purposes, mostly for DX9 SM2.0 level.
    // Most of the math is being done on these (1-x) values, and that saves a few precious ALU slots.
    half oneMinusReflectivity, smoothness;
    float3 normalWorld;
    float3 eyeVec;
    half alpha;
    float3 posWorld;


    half3 tangentSpaceNormal;
// #if UNITY_STANDARD_SIMPLE
//     half3 tangentSpaceNormal;
// #endif
};

half2 GetLayer0Texcoord(half4 i_tex, half4 detailUV){
	#if   _Layer0UVSet_0
		return i_tex.xy;
	#elif _Layer0UVSet_1
		return i_tex.zw;
	#elif _Layer0UVSet_2
		return detailUV.xy;	
	#elif _Layer0UVSet_3
		return detailUV.zw;	
	#else
		return i_tex.xy;
	#endif
}
half2 GetLayer1Texcoord(half4 i_tex, half4 detailUV){
	#if _Layer1UVSet_0
		return i_tex.xy;
	#elif _Layer1UVSet_1
		return i_tex.zw;
	#elif (_Layer1UVSet_2 )
		return detailUV.xy;
	#elif _Layer1UVSet_3
		return detailUV.zw;	
	#else 
		return i_tex.xy;
	#endif
}

half2 GetLayer2Texcoord(half4 i_tex, half4 detailUV){
	#if _Layer2UVSet_0
		return i_tex.xy;
	#elif _Layer2UVSet_1
		return i_tex.zw;
	#elif _Layer2UVSet_2
		return detailUV.xy;	
	#elif _Layer2UVSet_3
		return detailUV.zw;	
	#else 
		return i_tex.xy;
	#endif
}


half2 GetLayer3Texcoord(half4 i_tex, half4 detailUV){
	#if _Layer3UVSet_0
		return i_tex.xy;
	#elif _Layer3UVSet_1
		return i_tex.zw;
	#elif _Layer3UVSet_2
		return detailUV.xy;
	#elif _Layer3UVSet_3
		return detailUV.zw;
	#else 
		return i_tex.xy;
	#endif
}


half2 GetLayer4Texcoord(half4 i_tex, half4 detailUV){
	#if _Layer4UVSet_0
		return i_tex.xy;
	#elif _Layer4UVSet_1
		return i_tex.zw;
	#elif _Layer4UVSet_2
		return detailUV.xy;
	#elif _Layer4UVSet_3
		return detailUV.zw;
	#else 
		return i_tex.xy;
	#endif
}


half2 GetLayer5Texcoord(half4 i_tex, half4 detailUV){
	#if _Layer5UVSet_0
		return i_tex.xy;
	#elif _Layer5UVSet_1
		return i_tex.zw;
	#elif _Layer5UVSet_2
		return detailUV.xy;
	#elif _Layer5UVSet_3
		return detailUV.zw;
	#else 
		return i_tex.xy;
	#endif
}


half2 GetLayer6Texcoord(half4 i_tex, half4 detailUV){
	#if _Layer6UVSet_0
		return i_tex.xy;
	#elif _Layer6UVSet_1
		return i_tex.zw;
	#elif _Layer6UVSet_2
		return detailUV.xy;
	#elif _Layer6UVSet_3
		return detailUV.zw;
	#else 
		return i_tex.xy;
	#endif
}


half2 GetLayer7Texcoord(half4 i_tex, half4 detailUV){
	#if _Layer7UVSet_0
		return i_tex.xy;
	#elif _Layer7UVSet_1
		return i_tex.zw;
	#elif _Layer7UVSet_2
		return detailUV.xy;
	#elif _Layer7UVSet_3
		return detailUV.zw;	
	#else 
		return i_tex.xy;
	#endif
}

half2 GetLayer0Mask(half2 layer0UV){
	#if _LAYER_0_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer0Tex, layer0UV).a;
		return half2(lerp(1,0,step(a,0)), a);		
	#else
		return half2(1,1);
	#endif
}

half2 GetLayer1Mask(half2 layer1UV){
	#if _LAYER_1_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer1Tex, layer1UV).a;
		return half2(lerp(1,0,step(a,0)), a);		
	#else
		return half2(1,1);
	#endif
}

half2 GetLayer2Mask(half2 layer2UV){
	#if _LAYER_2_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer2Tex, layer2UV).a;
		return half2(lerp(1,0,step(a,0)), a);
	#else
		return half2(1,1);
	#endif
}


half2 GetLayer3Mask(half2 layer3UV){
	#if _LAYER_3_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer3Tex, layer3UV).a;
		return half2(lerp(1,0,step(a,0)), a);	
	#else
		return half2(1,1);
	#endif
}


half2 GetLayer4Mask(half2 layer4UV){
	#if _LAYER_4_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer4Tex, layer4UV).a;
		return half2(lerp(1,0,step(a,0)), a);	
	#else
		return half2(1,1);
	#endif
}

half2 GetLayer5Mask(half2 layer5UV){
	#if _LAYER_5_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer5Tex, layer5UV).a;
		return half2(lerp(1,0,step(a,0)), a);	
	#else
		return half2(1,1);
	#endif
}

half2 GetLayer6Mask(half2 layer6UV){
	#if _LAYER_6_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer6Tex, layer6UV).a;
		return half2(lerp(1,0,step(a,0)), a);	
	#else
		return half2(1,1);
	#endif
}

half2 GetLayer7Mask(half2 layer7UV){
	#if _LAYER_7_TEX 
		half a = SAMPLE_TEXTURE2D_Default(_Layer7Tex, layer7UV).a;
		return half2(lerp(1,0,step(a,0)), a);	
	#else
		return half2(1,1);
	#endif
}


// 漫反射比例 = 1 - 反射比例 
inline half OneMinusReflectivityFromMetallic(half metallic)
{
    // We'll need oneMinusReflectivity, so
    //   1-reflectivity = 1-lerp(dielectricSpec, 1, metallic) = lerp(1-dielectricSpec, 0, metallic)
    // store (1-dielectricSpec) in unity_ColorSpaceDielectricSpec.a, then
    //   1-reflectivity = lerp(alpha, 0, metallic) = alpha + metallic*(0 - alpha) =
    //                  = alpha - metallic * alpha
    // unity_ColorSpaceDielectricSpec 定义了绝缘体高光颜色喝反射率是一个经验值，Linear Space half4(0.04, 0.04, 0.04, 1.0 - 0.04)
    // Gamma Space half4(0.220916301, 0.220916301, 0.220916301, 1.0 - 0.220916301)
    half oneMinusDielectricSpec = 1-0.04;
    return oneMinusDielectricSpec - metallic * oneMinusDielectricSpec;
    // gamma空间 return 0.79*(1-metallic);  
    // Linear空间 0.96*(1-metallic); 
}

inline half3 DiffuseAndSpecularFromMetallic (half3 albedo, half metallic, out half3 specColor, out half oneMinusReflectivity)
{
    specColor = lerp (half3(0.04,0.04,0.04), albedo, metallic); 
    // 当金属度为1时候，specColor就是物体本身颜色。
    // 当金属度为0的时候,specColor几乎没有, 非金属的东西基本是diffuseColor。
    oneMinusReflectivity = OneMinusReflectivityFromMetallic(metallic);
    return albedo ; // albedo * 漫反射比例
    // return albedo * oneMinusReflectivity; // albedo * 漫反射比例
}

// Unity Blend
half3 BlendNormals(half3 n1, half3 n2)
{
    return normalize(half3(n1.xy + n2.xy, n1.z*n2.z));
    // return normalize(half3(n1.xy + n2.xy, n1.z)); // UDN
}

half3 UnpackScaleNormalRGorAG(half4 packednormal, half bumpScale)
{
    #if defined(UNITY_NO_DXT5nm)
        half3 normal = packednormal.xyz * 2 - 1;
        #if (SHADER_TARGET >= 30)
            // SM2.0: instruction count limitation
            // SM2.0: normal scaler is not supported
            normal.xy *= bumpScale;
        #endif
        return normal;
    #else
        // This do the trick
        packednormal.x *= packednormal.w;
        half3 normal;
        normal.xy = (packednormal.xy * 2 - 1);
        #if (SHADER_TARGET >= 30)
            // SM2.0: instruction count limitation
            // SM2.0: normal scaler is not supported
            normal.xy *= bumpScale;
        #endif
        normal.z = sqrt(1.0 - saturate(dot(normal.xy, normal.xy)));
        return normal;
    #endif
}

half3 UnpackScaleNormal(half4 packednormal, half bumpScale)
{
    return UnpackScaleNormalRGorAG(packednormal, bumpScale);
}

half3 UDNBlendNormal(half3 n1,half3 n2, half mask)
{
	return lerp(n1, BlendNormals(n1, n2), mask);
}

half BlendValue(half a, half b, half t)
{
	return lerp(a,b,t);
}

half3 UDNBlendNormal(half3 n1,half3 n2)
{
	return BlendNormals(n1, n2);
}
// 法线混合+遮罩 zzz
half3 BlendNormalTS(half3 normalTangent, half3 detailNormalTangent, half mask){

    //#if _DETAIL_LERP
    //    normalTangent = lerp(
    //        normalTangent,
    //        detailNormalTangent,
    //        mask);
    //#else
        // normalTangent = lerp(
        //     normalTangent,
        //     BlendNormals(normalTangent, detailNormalTangent),
        //     mask);
    //#endif	
		// mask = lerp(1,0,step(mask,0));
        // normalTangent = lerp(normalTangent,BlendNormals(normalTangent, detailNormalTangent),mask); // 混合
		
		normalTangent = lerp(normalTangent,detailNormalTangent,mask); // 覆盖
    //#endif
	return normalTangent;
}

// 妆容混合颜色, 应该不能直接覆盖
half4 BlendColor_Layer(half4 srcCol, half4 outCol) {

	// #if _Layer2_Blend_Normal
		// outCol.rgb = srcCol.rgb * srcCol.a + outCol.rgb * (1 - srcCol.a);
	// #elif _Layer2_Blend_Multiply
		outCol.rgb = lerp(outCol.rgb, srcCol.rgb, srcCol.a);		
	// #endif

	outCol.a = saturate(outCol.a + srcCol.a);
	return outCol;
}


void InitializeSkinSurfaceData(half4 uv01, half4 uv23, half3 albedoColor,half3 normalTS,half smoothness, out F_SurfaceData outSurfaceData)
{
    // half4 albedoAlpha = SampleAlbedoAlpha(uv01*_BaseMap_ST.xy + _BaseMap_ST.zw, TEXTURE2D_ARGS(_BaseMap, sampler_BaseMap));
    // _BaseColor.a = lerp(_MinTransparency, 1.0)
    // outSurfaceData.alpha = Alpha(albedoAlpha.a, _BaseColor, _Cutoff); // SurfaceInput.hlsl    
    
    // half4 specGloss = SampleMetallicSpecGloss(uv01, albedoAlpha.a);    
    half3 outCol = albedoColor;
    half outMetallic = 0;
    // half outSmoothness = specGloss.a;    
    half outSmoothness = smoothness;    
    half3 outNormalTS = normalTS;
    half3 InitNormal;
    
    half temp;

    #if (_LAYER_0_TEX || _LAYER_0_NORMAL)
		half2 layer0SourceUV = GetLayer0Texcoord(uv01, uv23);
        #if _LAYER_0_TEX
			half2 layer0UV = layer0SourceUV * _Layer0Tex_ST.xy + _Layer0Tex_ST.zw;
            half4 layer0TexCol = SAMPLE_TEXTURE2D_Default (_Layer0Tex, layer0UV);	
		#elif _LAYER_0_NORMAL
			half2 layer0UV = layer0SourceUV * _Layer0Normal_ST.xy + _Layer0Normal_ST.zw;
            half4 layer0TexCol = half4(1,1,1,1);
		#endif

		half2 layer0Mask = GetLayer0Mask(layer0UV);		                
		layer0TexCol = layer0TexCol*layer0Mask.x; 			

        // #ifdef _LAYER_0_TEX
		//     outCol.rgb = lerp(outCol.rgb, layer0TexCol*_Layer0Color.rgb,layer0TexCol.a*_Layer0Color.a); 
        // #endif
        layer0Mask.x *= layer0TexCol.w;
        #if _Layer0NormalMode_0
            outMetallic +=  _Layer0Metallic * layer0TexCol.a;
		    outSmoothness += _Layer0Smoothness * layer0TexCol.a;
            #ifdef _LAYER_0_TEX
                outCol.rgb = lerp(outCol.rgb, layer0TexCol*_Layer0Color.rgb,layer0TexCol.a*_Layer0Color.a);             
            #endif
        #elif _Layer0NormalMode_1
            temp = _Layer0Metallic * layer0TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer0Mask);
            temp = _Layer0Smoothness * layer0TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer0Mask);
            #ifdef _LAYER_0_TEX
                outCol.rgb = lerp(outCol.rgb, layer0TexCol*_Layer0Color.rgb,layer0TexCol.a*_Layer0Color.a);
                outCol.rgb = lerp(outCol.rgb,  layer0TexCol*_Layer0Color.rgb, layer0Mask.x);
            #endif
        #endif
		
        // Normal Blend Model
        #if _LAYER_0_NORMAL
            half3 layer0NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer0Normal, layer0UV), _Layer0NormalScale);            
            #if _Layer0NormalMode_0
                #if _LAYER_0_TEX
                    InitNormal = outNormalTS;
                    outNormalTS = UDNBlendNormal(outNormalTS, layer0NormalTS);
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer0Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer0NormalTS);
                #endif
            #elif _Layer0NormalMode_1
                outNormalTS = BlendNormalTS(outNormalTS, layer0NormalTS, layer0Mask.x);
            #endif
        #endif
	#endif

    #if (_LAYER_1_TEX || _LAYER_1_NORMAL)
		half2 layer1SourceUV = GetLayer1Texcoord(uv01, uv23);
        #if _LAYER_1_TEX
			half2 layer1UV = layer1SourceUV * _Layer1Tex_ST.xy + _Layer1Tex_ST.zw;
            half4 layer1TexCol = SAMPLE_TEXTURE2D_Default (_Layer1Tex, layer1UV);	
		#elif _LAYER_1_NORMAL
			half2 layer1UV = layer1SourceUV * _Layer1Normal_ST.xy + _Layer1Normal_ST.zw;
            half4 layer1TexCol = half4(1,1,1,1);
		#endif		
		half2 layer1Mask = GetLayer1Mask(layer1UV);		        

        // Layer1 AlphaTest
        // #if defined(_ALPHATEST_ON)
        //     // half _alpha = _BaseColor.a*layer1TexCol.a*_Layer1Color.a;
        //     half _alpha = _BaseColor.a*_Layer1Color.a;
        //     _alpha = lerp(_MinTransparency, 1 ,_alpha*layer1TexCol.a*_Transparency);
        //     clip(_alpha - _Layer1Cutoff);
        //     // outSurfaceData.alpha = DoPreFunction(input, outSurfaceData.alpha);
        //     outSurfaceData.alpha = DoPreFunction(input, _alpha);            
        // #endif
        
		layer1TexCol = layer1TexCol*layer1Mask.x;
        // #ifdef _LAYER_1_TEX
		//     outCol.rgb = lerp(outCol.rgb, layer1TexCol*_Layer1Color.rgb,layer1TexCol.a*_Layer1Color.a); 
        // #endif

        

        layer1Mask.x *= layer1TexCol.w;
        #if _Layer1NormalMode_0
            outMetallic +=  _Layer1Metallic * layer1TexCol.a;
		    outSmoothness += _Layer1Smoothness * layer1TexCol.a;
            #ifdef _LAYER_1_TEX
                outCol.rgb = lerp(outCol.rgb, layer1TexCol*_Layer1Color.rgb,layer1TexCol.a*_Layer1Color.a);                 
            #endif
        #elif _Layer1NormalMode_1            		    
            temp = _Layer1Metallic * layer1TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer1Mask);
            temp = _Layer1Smoothness * layer1TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer1Mask);
            #ifdef _LAYER_1_TEX
                outCol.rgb = lerp(outCol.rgb, layer1TexCol*_Layer1Color.rgb,layer1TexCol.a*_Layer1Color.a); 
                outCol.rgb = lerp(outCol.rgb,  layer1TexCol*_Layer1Color.rgb, layer1Mask.x);
            #endif
        #endif
		

        // Normal Blend Model
        #if _LAYER_1_NORMAL
            half3 layer1NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer1Normal, layer1UV), _Layer1NormalScale);            
            #if _Layer1NormalMode_0
                #if _LAYER_1_TEX
                    InitNormal = outNormalTS;                                    
                    outNormalTS = UDNBlendNormal(outNormalTS, layer1NormalTS,layer1Mask.x);
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer1Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer1NormalTS);
                #endif
            #elif _Layer1NormalMode_1
                outNormalTS = BlendNormalTS(outNormalTS, layer1NormalTS, layer1Mask.x);
                // InitNormal = outNormalTS;
                // outNormalTS = BlendNormalTS(outNormalTS, layer1NormalTS, layer1Mask.x);
                // _mask = layer1TexCol.a*(1-layer1Mask.y);
                // outNormalTS = lerp(BlendNormalTS(outNormalTS,InitNormal, _mask),outNormalTS,step(_mask,0));
            #endif            
        #endif
	#endif

    #if (_LAYER_2_TEX || _LAYER_2_NORMAL)
		half2 layer2SourceUV = GetLayer2Texcoord(uv01, uv23);
        #if _LAYER_2_TEX
			half2 layer2UV = layer2SourceUV * _Layer2Tex_ST.xy + _Layer2Tex_ST.zw;
            half4 layer2TexCol = SAMPLE_TEXTURE2D_Default (_Layer2Tex, layer2UV);	
		#elif _LAYER_2_NORMAL
			half2 layer2UV = layer2SourceUV * _Layer2Normal_ST.xy + _Layer2Normal_ST.zw;
            half4 layer2TexCol = half4(1,1,1,1);
		#endif

		half2 layer2Mask = GetLayer2Mask(layer2UV);		                
		layer2TexCol = layer2TexCol*layer2Mask.x; 			

        // #ifdef _LAYER_2_TEX
		//     outCol.rgb = lerp(outCol.rgb, layer2TexCol*_Layer2Color.rgb,layer2TexCol.a*_Layer2Color.a); 
        // #endif

        layer2Mask.x *= layer2TexCol.w;
        #if _Layer2NormalMode_0
            outMetallic +=  _Layer2Metallic * layer2TexCol.a;
		    outSmoothness += _Layer2Smoothness * layer2TexCol.a;
            #ifdef _LAYER_2_TEX
                outCol.rgb = lerp(outCol.rgb, layer2TexCol*_Layer2Color.rgb,layer2TexCol.a*_Layer2Color.a);                 
            #endif
        #elif _Layer2NormalMode_1            		    
            temp = _Layer2Metallic * layer2TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer2Mask);
            temp = _Layer2Smoothness * layer2TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer2Mask);

            #ifdef _LAYER_2_TEX
                outCol.rgb = lerp(outCol.rgb, layer2TexCol*_Layer2Color.rgb,layer2TexCol.a*_Layer2Color.a); 
                outCol.rgb = lerp(outCol.rgb,  layer2TexCol*_Layer2Color.rgb, layer2Mask.x);
            #endif
        #endif		

        // Normal Blend Model
        #if _LAYER_2_NORMAL
            half3 layer2NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer2Normal, layer2UV), _Layer2NormalScale);            
            #if _Layer2NormalMode_0
                #if _LAYER_2_TEX
                    InitNormal = outNormalTS;
                    outNormalTS = UDNBlendNormal(outNormalTS, layer2NormalTS);
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer2Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer2NormalTS);
                #endif
            #elif _Layer2NormalMode_1
                outNormalTS = BlendNormalTS(outNormalTS, layer2NormalTS, layer2Mask.x);
            #endif
        #endif
	#endif


    #if (_LAYER_3_TEX || _LAYER_3_NORMAL)
		half2 layer3SourceUV = GetLayer3Texcoord(uv01, uv23);
        #if _LAYER_3_TEX
			half2 layer3UV = layer3SourceUV * _Layer3Tex_ST.xy + _Layer3Tex_ST.zw;
            half4 layer3TexCol = SAMPLE_TEXTURE2D_Default (_Layer3Tex, layer3UV);	
		#elif _LAYER_3_NORMAL
			half2 layer3UV = layer3SourceUV * _Layer3Normal_ST.xy + _Layer3Normal_ST.zw;
            half4 layer3TexCol = half4(1,1,1,1);
		#endif		
		half2 layer3Mask = GetLayer3Mask(layer3UV);		
        
        
		layer3TexCol = layer3TexCol*layer3Mask.x;
        // #ifdef _LAYER_3_TEX
		//     outCol.rgb = lerp(outCol.rgb, layer3TexCol*_Layer3Color.rgb,layer3TexCol.a*_Layer3Color.a); 
        // #endif

        layer3Mask.x *= layer3TexCol.w;
        #if _Layer3NormalMode_0
            outMetallic +=  _Layer3Metallic * layer3TexCol.a;
		    outSmoothness += _Layer3Smoothness * layer3TexCol.a;

            #ifdef _LAYER_3_TEX
                outCol.rgb = lerp(outCol.rgb, layer3TexCol*_Layer3Color.rgb,layer3TexCol.a*_Layer3Color.a);                 
            #endif
        #elif _Layer3NormalMode_1            		    
            temp = _Layer3Metallic * layer3TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer3Mask);
            temp = _Layer3Smoothness * layer3TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer3Mask);

            #ifdef _LAYER_3_TEX
                outCol.rgb = lerp(outCol.rgb, layer3TexCol*_Layer3Color.rgb,layer3TexCol.a*_Layer3Color.a); 
                outCol.rgb = lerp(outCol.rgb,  layer3TexCol*_Layer3Color.rgb, layer3Mask.x);
            #endif
        #endif		
		
        // Normal Blend Model
        #if _LAYER_3_NORMAL
            half3 layer3NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer3Normal, layer3UV), _Layer3NormalScale);            
            #if _Layer3NormalMode_0
                #if _LAYER_3_TEX
                    InitNormal = outNormalTS;                                    
                    outNormalTS = UDNBlendNormal(outNormalTS, layer3NormalTS);                
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer3Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer3NormalTS);
                #endif
            #elif _Layer3NormalMode_1                
                outNormalTS = BlendNormalTS(outNormalTS, layer3NormalTS, layer3Mask.x);
            #endif            
        #endif                
	#endif


    #if (_LAYER_4_TEX || _LAYER_4_NORMAL)
		half2 layer4SourceUV = GetLayer4Texcoord(uv01, uv23);
        #if _LAYER_4_TEX
			half2 layer4UV = layer4SourceUV * _Layer4Tex_ST.xy + _Layer4Tex_ST.zw;
            half4 layer4TexCol = SAMPLE_TEXTURE2D_Default (_Layer4Tex, layer4UV);	
		#elif _LAYER_4_NORMAL
			half2 layer4UV = layer4SourceUV * _Layer4Normal_ST.xy + _Layer4Normal_ST.zw;
            half4 layer4TexCol = half4(1,1,1,1);
		#endif		
		half2 layer4Mask = GetLayer4Mask(layer4UV);		
        
        
		layer4TexCol = layer4TexCol*layer4Mask.x;
        // #ifdef _LAYER_4_TEX
		//     outCol.rgb = lerp(outCol.rgb, layer4TexCol*_Layer4Color.rgb,layer4TexCol.a*_Layer4Color.a); 
        // #endif

        layer4Mask.x *= layer4TexCol.w;
        #if _Layer4NormalMode_0
            outMetallic +=  _Layer4Metallic * layer4TexCol.a;
		    outSmoothness += _Layer4Smoothness * layer4TexCol.a;

            #ifdef _LAYER_4_TEX
                outCol.rgb = lerp(outCol.rgb, layer4TexCol*_Layer4Color.rgb,layer4TexCol.a*_Layer4Color.a); 
            #endif
            
        #elif _Layer4NormalMode_1            		    
            temp = _Layer4Metallic * layer4TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer4Mask);
            temp = _Layer4Smoothness * layer4TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer4Mask);

            #ifdef _LAYER_4_TEX
                outCol.rgb = lerp(outCol.rgb, layer4TexCol*_Layer4Color.rgb,layer4TexCol.a*_Layer4Color.a);
                outCol.rgb = lerp(outCol.rgb, layer4TexCol*_Layer4Color.rgb, layer4Mask.x);
            #endif
        #endif			

        // Normal Blend Model
        #if _LAYER_4_NORMAL
            half3 layer4NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer4Normal, layer4UV), _Layer4NormalScale);            
            #if _Layer4NormalMode_0
                #if _LAYER_4_TEX
                    InitNormal = outNormalTS;                                    
                    outNormalTS = UDNBlendNormal(outNormalTS, layer4NormalTS);                
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer4Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer4NormalTS);
                #endif
            #elif _Layer4NormalMode_1                
                outNormalTS = BlendNormalTS(outNormalTS, layer4NormalTS, layer4Mask.x);
            #endif            
        #endif                
	#endif

    #if (_LAYER_5_TEX || _LAYER_5_NORMAL)
		half2 layer5SourceUV = GetLayer5Texcoord(uv01, uv23);
        #if _LAYER_5_TEX
			half2 layer5UV = layer5SourceUV * _Layer5Tex_ST.xy + _Layer5Tex_ST.zw;
            half4 layer5TexCol = SAMPLE_TEXTURE2D_Default (_Layer5Tex, layer5UV);	
		#elif _LAYER_5_NORMAL
			half2 layer5UV = layer5SourceUV * _Layer5Normal_ST.xy + _Layer5Normal_ST.zw;
            half4 layer5TexCol = half4(1,1,1,1);
		#endif		
		half2 layer5Mask = GetLayer5Mask(layer5UV);		
        
        
		layer5TexCol = layer5TexCol*layer5Mask.x;
        // #ifdef _LAYER_5_TEX 			
		//     outCol.rgb = lerp(outCol.rgb, layer5TexCol*_Layer5Color.rgb,layer5TexCol.a*_Layer5Color.a); 
        // #endif

        layer5Mask.x *= layer5TexCol.w;
        #if _Layer5NormalMode_0
            outMetallic +=  _Layer5Metallic * layer5TexCol.a;
		    outSmoothness += _Layer5Smoothness * layer5TexCol.a;
            #ifdef _LAYER_5_TEX
                outCol.rgb = lerp(outCol.rgb, layer5TexCol*_Layer5Color.rgb,layer5TexCol.a*_Layer5Color.a); 
            #endif
        #elif _Layer5NormalMode_1            		    
            temp = _Layer5Metallic * layer5TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer5Mask);
            temp = _Layer5Smoothness * layer5TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer5Mask);
            #ifdef _LAYER_5_TEX
                outCol.rgb = lerp(outCol.rgb, layer5TexCol*_Layer5Color.rgb,layer5TexCol.a*_Layer5Color.a);
                outCol.rgb = lerp(outCol.rgb, layer5TexCol*_Layer5Color.rgb, layer5Mask.x);
            #endif
        #endif	
		

        // Normal Blend Model
        #if _LAYER_5_NORMAL
            half3 layer5NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer5Normal, layer5UV), _Layer5NormalScale);            
            #if _Layer5NormalMode_0
                #if _LAYER_5_TEX
                    InitNormal = outNormalTS;                                    
                    outNormalTS = UDNBlendNormal(outNormalTS, layer5NormalTS);                
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer5Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer5NormalTS);
                #endif
            #elif _Layer5NormalMode_1                
                outNormalTS = BlendNormalTS(outNormalTS, layer5NormalTS, layer5Mask.x);
            #endif            
        #endif                
	#endif

    #if (_LAYER_6_TEX || _LAYER_6_NORMAL)
		half2 layer6SourceUV = GetLayer6Texcoord(uv01, uv23);
        #if _LAYER_6_TEX
			half2 layer6UV = layer6SourceUV * _Layer6Tex_ST.xy + _Layer6Tex_ST.zw;
            half4 layer6TexCol = SAMPLE_TEXTURE2D_Default (_Layer6Tex, layer6UV);	
		#elif _LAYER_6_NORMAL
			half2 layer6UV = layer6SourceUV * _Layer6Normal_ST.xy + _Layer6Normal_ST.zw;
            half4 layer6TexCol = half4(1,1,1,1);
		#endif		
		half2 layer6Mask = GetLayer6Mask(layer6UV);		
        
        
		layer6TexCol = layer6TexCol*layer6Mask.x;
        // #ifdef _LAYER_6_TEX
		//     outCol.rgb = lerp(outCol.rgb, layer6TexCol*_Layer6Color.rgb,layer6TexCol.a*_Layer6Color.a); 
        // #endif

        layer6Mask.x *= layer6TexCol.w;
        #if _Layer6NormalMode_0
            outMetallic +=  _Layer6Metallic * layer6TexCol.a;
		    outSmoothness += _Layer6Smoothness * layer6TexCol.a;
            #ifdef _LAYER_6_TEX
                outCol.rgb = lerp(outCol.rgb, layer6TexCol*_Layer6Color.rgb,layer6TexCol.a*_Layer6Color.a); 
            #endif
        #elif _Layer6NormalMode_1            		    
            temp = _Layer6Metallic * layer6TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer6Mask);
            temp = _Layer6Smoothness * layer6TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer6Mask);            
            #ifdef _LAYER_6_TEX
                outCol.rgb = lerp(outCol.rgb, layer6TexCol*_Layer6Color.rgb,layer6TexCol.a*_Layer6Color.a); 
                outCol.rgb = lerp(outCol.rgb,  layer6TexCol*_Layer6Color.rgb, layer6Mask.x);
            #endif
        #endif	

        // Normal Blend Model
        #if _LAYER_6_NORMAL
            half3 layer6NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer6Normal, layer6UV), _Layer6NormalScale);            
            #if _Layer6NormalMode_0
                #if _LAYER_6_TEX
                    InitNormal = outNormalTS;                                    
                    outNormalTS = UDNBlendNormal(outNormalTS, layer6NormalTS);                
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer6Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer6NormalTS);
                #endif
            #elif _Layer6NormalMode_1                
                outNormalTS = BlendNormalTS(outNormalTS, layer6NormalTS, layer6Mask.x);
            #endif            
        #endif                
	#endif

    #if (_LAYER_7_TEX || _LAYER_7_NORMAL)		
        half2 layer7SourceUV = GetLayer7Texcoord(uv01, uv23);
        #if _LAYER_7_TEX
			half2 layer7UV = layer7SourceUV * _Layer7Tex_ST.xy + _Layer7Tex_ST.zw;
            half4 layer7TexCol = SAMPLE_TEXTURE2D_Default (_Layer7Tex, layer7UV);	
		#elif _LAYER_7_NORMAL
			half2 layer7UV = layer7SourceUV * _Layer7Normal_ST.xy + _Layer7Normal_ST.zw;
            half4 layer7TexCol = half4(1,1,1,1);
		#endif		
		half2 layer7Mask = GetLayer7Mask(layer7UV);		
        
        
		layer7TexCol = layer7TexCol*layer7Mask.x;
        // #ifdef _LAYER_7_TEX // #ifndef
		//     outCol.rgb = lerp(outCol.rgb, layer7TexCol*_Layer7Color.rgb,layer7TexCol.a*_Layer7Color.a); 
        // #endif
        
		// #if _LAYER_7_MASK 
		// 	half _layer7Mask = SAMPLE_TEXTURE2D_Default (_Layer7Mask, layer7UV);			
		// 	_layer7Mask = lerp(7,0,step(_layer7Mask,0)); 
		// 	layer7TexCol.a *= layer7Mask.x* _layer7Mask;
		// #endif

        layer7Mask.x *= layer7TexCol.w;
        #if _Layer7NormalMode_0
            outMetallic +=  _Layer7Metallic * layer7TexCol.a;
		    outSmoothness += _Layer7Smoothness * layer7TexCol.a;
            #ifdef _LAYER_7_TEX
                outCol.rgb = lerp(outCol.rgb, layer7TexCol*_Layer7Color.rgb,layer7TexCol.a*_Layer7Color.a);             
            #endif
        #elif _Layer7NormalMode_1            		    
            temp = _Layer7Metallic * layer7TexCol.a;
            outMetallic = BlendValue(outMetallic, temp, layer7Mask);
            temp = _Layer7Smoothness * layer7TexCol.a;
            outSmoothness = BlendValue(outSmoothness, temp, layer7Mask);

            #ifdef _LAYER_7_TEX
                outCol.rgb = lerp(outCol.rgb, layer7TexCol*_Layer7Color.rgb,layer7TexCol.a*_Layer7Color.a); 
                outCol.rgb = lerp(outCol.rgb,  layer7TexCol*_Layer7Color.rgb, layer7Mask.x);
            #endif
        #endif	

        // Normal Blend Model
        #if _LAYER_7_NORMAL
            half3 layer7NormalTS = UnpackScaleNormal(SAMPLE_TEXTURE2D_Default (_Layer7Normal, layer7UV), _Layer7NormalScale);            
            #if _Layer7NormalMode_0
                #if _LAYER_7_TEX
                    InitNormal = outNormalTS;                                    
                    outNormalTS = UDNBlendNormal(outNormalTS, layer7NormalTS);
                    outNormalTS = BlendNormalTS(InitNormal, outNormalTS, layer7Mask.x);
                #else
                    outNormalTS = UDNBlendNormal(outNormalTS, layer7NormalTS);
                #endif
            #elif _Layer7NormalMode_1                
                outNormalTS = BlendNormalTS(outNormalTS, layer7NormalTS, layer7Mask.x);                
            #endif            
        #endif                
	#endif

  
    
#if _SPECULAR_SETUP
    outSurfaceData.metallic = 1.0h;
    outSurfaceData.specular = specGloss.rgb;
#else
    outSurfaceData.metallic = outMetallic;
    outSurfaceData.specular = half3(0.0h, 0.0h, 0.0h);
#endif

    outSurfaceData.smoothness = outSmoothness;

    outSurfaceData.albedo = outCol;
    outSurfaceData.normalTS = outNormalTS;
    
    outSurfaceData.occlusion = 1.0h;
    outSurfaceData.emission = half3(1,1,1);
    outSurfaceData.alpha = 1.0;
    outSurfaceData.specular = half3(1,1,1);

}


#endif