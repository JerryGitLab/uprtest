﻿Shader "CustomURP/URP_LowShadow"
{
	Properties
	{
		_Color("Color", COLOR) = (.172, .463, .435, 1)						
		[NoScaleOffset] _MainTex("Main Tex", 2D) = "white" {}		
	}
	SubShader
	{
		
		// Tags{"RenderType" = "Transparent"  "Queue" = "Transparent"  "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		// Tags{"RenderType" = "Opaque"  "Queue" = "Transparent"  "RenderPipeline" = "UniversalPipeline" "IgnoreProjector" = "True"}
		Tags{"RenderType" = "Transparent" "Queue" = "Geometry+1" "RenderPipeline" = "UniversalPipeline"}
		Pass
		{
			Name "LQShadow"
			Tags { "LightMode"="UniversalForward" }

			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			ZTest  Always
			Cull front
			HLSLPROGRAM
			#pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 3.5
			#pragma multi_compile_instancing

			#pragma vertex vert
			#pragma fragment frag
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			struct Attributes
			{
				float4 positionOS	: POSITION;
				float2 uv			: TEXCOORD0;
				float3 normal		: NORMAL;
			};

			struct Varyings
			{
				float4 positionHCS  : SV_POSITION;
				float4 uv           : TEXCOORD0;
				float4 positionProj  : TEXCOORD1;
			};									
			TEXTURE2D(_CameraDepthTexture);       SAMPLER(sampler_CameraDepthTexture);
			TEXTURE2D(_MainTex);       SAMPLER(sampler_MainTex);
			float4 _Color;
			float4 ComputeNDCPos (float4 pos)
			{
				float scale = 1.0;
				#if UNITY_UV_STARTS_AT_TOP
					scale = -1.0;
				#endif				
				float4 o = pos * 0.5f;
				o.xy = float2(o.x, o.y*scale) + o.w;
				o.zw = pos.zw;
				return o;
			}

			float4 GetWorldPosByDepth(float4 screenProj)
			{
				float2 screenUV = screenProj.xy / screenProj.w;								
				float depth = SAMPLE_TEXTURE2D(_CameraDepthTexture, sampler_CameraDepthTexture, screenUV);			
				#if UNITY_UV_STARTS_AT_TOP
					depth = 1 - depth;									
				#endif
				float4 projPos = float4(0, 0, 0, 0);
				projPos = float4(screenUV.x * 2 - 1, screenUV.y * 2 - 1, depth * 2 - 1, 1);
				projPos = mul(unity_CameraInvProjection, projPos);

				float4 worldPos = mul(unity_MatrixInvV, projPos);
				worldPos = worldPos / worldPos.w;

				return worldPos;
			}

			Varyings vert(Attributes input)
			{
				Varyings o = (Varyings)0;				
				half3 positionWS = TransformObjectToWorld(input.positionOS.xyz);				
				o.positionHCS = TransformWorldToHClip(positionWS);
				o.positionProj = ComputeNDCPos(o.positionHCS);				
				return o;
			}

			half4 frag(Varyings i) : SV_Target
			{          
				float4 worldPos = GetWorldPosByDepth(i.positionProj);
				float4 modelPos = mul(unity_WorldToObject, worldPos);
				modelPos.xyz = modelPos.xyz / modelPos.w;
				float2 localUV = modelPos.xz + 0.5;
				half4 col = SAMPLE_TEXTURE2D(_MainTex,sampler_MainTex, localUV);
				col.a *= col.r;
				col.rgb *=_Color.rgb;
				return col;
			}
			ENDHLSL
		}				
	}
}