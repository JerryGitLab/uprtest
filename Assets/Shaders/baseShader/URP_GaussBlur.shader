﻿Shader "Custom/URP_GaussBlur"
{
    Properties
    {
        _MainTex("MainTex",2D) = "white"{}
        _BlurRadius1("_BlurRadius",Vector) = (1,2,0,0)
    }

    HLSLINCLUDE
    #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
    sampler2D _MainTex;    
    float2 _BlurRadius1;
    float4 _MainTex_TexelSize;

    struct Attributes
    {
        float4 positionOS: POSITION;
        float2 uv : TEXCOORD0;
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float4 uv0 : TEXCOORD0;
        float4 uv1 : TEXCOORD1;
        float4 uv2 : TEXCOORD2;
        float4 uv3 : TEXCOORD3;
    };


    Varyings vertX(Attributes v)
    {
        Varyings o = (Varyings)0;
        VertexPositionInputs vertexInput = GetVertexPositionInputs(v.positionOS.xyz);
        o.positionCS = vertexInput.positionCS;

        float offset = _MainTex_TexelSize.x * _BlurRadius1.x;
        o.uv0.xy = float2(-3,0)* offset + v.uv.xy;
        o.uv0.zw = float2(-2,0)* offset + v.uv.xy;

        o.uv1.xy = float2(-1,0)* offset + v.uv.xy;
        o.uv1.zw = float2(0,0) * offset + v.uv.xy;

        o.uv2.xy = float2(1,0) * offset + v.uv.xy;
        o.uv2.zw = float2(2,0) * offset + v.uv.xy;

        o.uv3.xy = float2(3,0) * offset + v.uv.xy;
        o.uv3.zw = float2(0,0);
        return o;
    }

 
    Varyings vertY(Attributes v)
    {
        Varyings o = (Varyings)0;
        VertexPositionInputs vertexInput = GetVertexPositionInputs(v.positionOS.xyz);
        o.positionCS = vertexInput.positionCS;

        float offset = _MainTex_TexelSize.y * _BlurRadius1.y;
        o.uv0.xy = float2(0,-3)* offset + v.uv.xy;
        o.uv0.zw = float2(0,-2)* offset + v.uv.xy;

        o.uv1.xy = float2(0,-1)* offset + v.uv.xy;
        o.uv1.zw = float2(0,0) * offset + v.uv.xy;

        o.uv2.xy = float2(0,1) * offset + v.uv.xy;
        o.uv2.zw = float2(0,2) * offset + v.uv.xy;

        o.uv3.xy = float2(0,3) * offset + v.uv.xy;
        o.uv3.zw = float2(0,0);
        return o;
    }



    float4 frag(Varyings i) : SV_Target
    {
        float4 baseColor = tex2D(_MainTex,i.uv0.zw);
        baseColor *= 0.0855000019;        
        float4 addColor = tex2D(_MainTex,i.uv0.xy);
        baseColor += addColor * 0.0205000006;
        
        addColor = tex2D(_MainTex,i.uv1.xy);
        baseColor += addColor * 0.231999993;        
        addColor = tex2D(_MainTex,i.uv1.zw);
        baseColor += addColor * 0.324000001;
        
        addColor = tex2D(_MainTex,i.uv2.xy);
        baseColor += addColor * 0.231999993;        
        addColor = tex2D(_MainTex,i.uv2.zw);
        baseColor += addColor * 0.0855000019;

        addColor = tex2D(_MainTex,i.uv3.xy);
        baseColor += addColor * 0.0205000006; 

        return baseColor;
    }
    
    ENDHLSL

    SubShader
    {
        Zwrite OFF    
        Ztest OFF
        Cull OFF
        Pass
        {
            Tags{"LightMode" = "UniversalForward" "RenderType" = "Opaque"}
            Name "firstGauss1"
            HLSLPROGRAM
            #pragma vertex vertX
            #pragma fragment frag
            ENDHLSL
        }

        Pass
        {
            Tags{"LightMode" = "LightweightForward"}
            Name "SecondGauss1"
            HLSLPROGRAM
            #pragma vertex vertY
            #pragma fragment frag
            ENDHLSL
        }

    }
}